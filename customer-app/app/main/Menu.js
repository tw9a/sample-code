import Page from 'components/Page'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import itemsService from 'services/itemsService'
import { filterInputBox } from '../../helpers/styling'
import ItemsByCat from 'components/ItemsByCat'
import { initCategories } from '../../reducers/categoriesReducer'
import { itemAddOne } from '../../reducers/itemsReducer'
import strings from '../../assets/strings'
// import { addItemToCat } from '../../reducers/itemsByCatsReducer'

const Menu = () => {
  const [catsInMenu, setCatsInMenu] = useState([])
  const [filterBy, setFilterBy] = useState('')
  const dispatch = useDispatch()
  // const allItems = useSelector(state => state.items)
  // const itemsByCats = useSelector( state=>state.itemsByCats)
  const orderType = useSelector(
    state => state.ordertype
  )
  const lang = useSelector(state => state.lang)

  // Download all categories from the server
  // and make a one-time initialization of the categories
  useEffect(
    () => {
      if (catsInMenu.length === 0) {
      itemsService.getAllCategories()
      .then(
        rawCats => {
          let redactedCats = rawCats.map(
            thisCat => {
              const newCat = {
                id: thisCat._id.$oid,
                cat: thisCat.cat,
                catText: thisCat.catText,
                type: thisCat.type
              }
              return newCat
          })
        dispatch(initCategories(redactedCats))
        setCatsInMenu(redactedCats)
        })}
      }, []) // eslint-disable-line
  
  // When categories reducer is initialized,
  // download all items from the server cat by cat.
  // It's easier to add items one by one.
  useEffect(
    ()=>{
      catsInMenu.forEach(
        thisCat => {
          itemsService.getItemsByCategory(thisCat.cat).then(
            // ret from `getItemsByCategory` is an array of items
            ret => {
              // console.log(ret)
              ret.forEach(
                thisItem => {
                  const itemId = thisItem._id.$oid
                  const newItem = {...thisItem, amount: 0}
                  newItem._id = itemId
                  dispatch(itemAddOne(newItem))
                }
              )
            }
          )
        }
      )
    }, [catsInMenu] // eslint-disable-line
  )


  const handleFilterChange = (e) => {
    setFilterBy(e.target.value)
  }

  return (
    <Page>
      <input placeholder={strings.placeholder_filter[lang]} name='filterBy' onChange={handleFilterChange} value={filterBy} style={filterInputBox} />
      {catsInMenu.map(
        cat => <ItemsByCat key={cat.id} catObj={cat} filterBy={filterBy} orderType={orderType} />
      )}
    </Page>
  )
}

export default Menu
