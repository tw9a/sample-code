import React from 'react';
import Page from 'components/Page'
import Accordion from '@material-ui/core/Accordion'
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import { useParams, useHistory } from "react-router-dom"
import { useSelector } from 'react-redux'

// TW0731
// Used files under /components/forms to serve for Accordions
import ReviewCart from '../../components/accordions/ReviewCart'
import ContactInfoDeliveryTime from '../../components/accordions/ContactAndDeliveryTime'
import PayForm from '../../components/accordions/PayForm'
import CheckoutComplete from '../../components/accordions/CheckoutComplete'
import strings from '../../assets/strings'

// TW0731
// Accepted steps are: review, contact, pay, complete
const Checkout = () => {
  const step = useParams().step
  const history = useHistory()
  const lang = useSelector( state=>state.lang )

  if (!['review', 'contact', 'pay', 'complete'].includes(step)) {
    history.push('/checkout/review')
  }

  // TODO: This can either be a download link
  // or a function that requests it.
  const downloadLink = ''

  return <Page>
    <div>
    <Accordion expanded={step==='review'}>
      <AccordionSummary>{strings.checkout_step1[lang]}</AccordionSummary>
      <AccordionDetails>
        <ReviewCart nextStep={()=>history.push('/checkout/contact')} />
      </AccordionDetails>
    </Accordion>
    <Accordion expanded={step==='contact'}>
      <AccordionSummary>{strings.checkout_step2[lang]}</AccordionSummary>
      <AccordionDetails>
        <ContactInfoDeliveryTime prevStep={()=>history.push('/checkout/review')} nextStep={()=>history.push('/checkout/pay')} />
      </AccordionDetails>
    </Accordion>
    <Accordion expanded={step==='pay'}>
      <AccordionSummary>{strings.checkout_step3[lang]}</AccordionSummary>
      <AccordionDetails>
        <PayForm prevStep={()=>history.push('/checkout/contact')} nextStep={()=>history.push('/checkout/complete')} />
      </AccordionDetails>
    </Accordion>
    <Accordion expanded={step==='complete'}>
      <AccordionSummary>{strings.checkout_step4[lang]}</AccordionSummary>
      <AccordionDetails><CheckoutComplete downloadLink={downloadLink} goToHome={()=>history.push('/')} /></AccordionDetails>
    </Accordion>
    </div>
  </Page>
}

export default Checkout