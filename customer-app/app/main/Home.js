import Page from 'components/Page'
import { Button, Container } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { ordertypeChange } from '../../reducers/ordertypeReducer'
import { useHistory } from "react-router-dom"
import backgroundImg from '../../assets/images/reactHomeContainerBgImg.jpg'
import strings from '../../assets/strings'

// TW0729
// When the user clicks Eat In or Take Away, 
// dispatch to redux and redirect page.

const Home = () => {
    const dispatch = useDispatch()
    const history = useHistory()
    const orderType = useSelector( state=>state.ordertype )
    // console.log('Home, before click, orderType is', orderType)
    const lang = useSelector( state=>state.lang )
    const homeContainerBgImg = {
        background: `url(${backgroundImg})`,
        alignItems: 'center',
        // TW0729
        // min-height is a mystery.
        // I tried `1vh` which is supposedly height of view port
        // but it produces a narrow strip.
        minHeight: '300px',
        // TW0729
        // valid options:
        // 'contain', 'cover'
        // specify 'no-repeat' when using contain
        backgroundSize: 'cover',
        display: 'flex',
        justifyContent: 'center' 
    }

    // TW0729
    // 1) When the button clicked is different from the ordertypeReducer, toggle it.
    // 2) redirect current page to menu.
    const handleClick = (name) => {
        if (name!==orderType) {
            dispatch(
                ordertypeChange()
            )
        }
        history.push('/menu')
    }

    return (
    <Page>
        {/* Rex changed from minWidth to maxWidth since there is no minWidth property in component <Contain /> */}
        <Container maxWidth="lg" fixed={true} style={homeContainerBgImg}>
        <Button variant="contained" onClick={()=>{handleClick('eat in')}}>{strings.menu_eatin[lang]}</Button>
        <Button variant="contained" onClick={()=>{handleClick('take away')}}>{strings.menu_takeaway[lang]}</Button>
        </Container>
    </Page>
    )
}

export default Home
