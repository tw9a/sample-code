import Page from 'components/Page'
import { useSelector } from 'react-redux'
import strings from '../../assets/strings'

const About = () => {
    const lang = useSelector( state=>state.lang )

    return (
        <Page>
            <div>{strings.aboutpage[lang]}</div>
        </Page>
    )
}

export default About