import React from 'react';
import './index.css';

import Home from './app/main/Home'
import Menu from './app/main/Menu'
import Contact from './app/main/Contact'
import About from './app/main/About'
import Login from './app/main/Login'
import Checkout from './app/main/Checkout'

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Container from '@material-ui/core/Container

const App = () => {
  return ( 
  <Container>
    <Router>
      <Switch>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/menu">
          <Menu />
        </Route>
        <Route path="/contact">
          <Contact />
        </Route>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/checkout/:step">
          <Checkout />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  </Container>
  )
}

export default App

