import { customAlphabet } from 'nanoid'

const nanoID32 = customAlphabet('0123456789abcdef', 32)

export default nanoID32