// TW0730 - Used in /components/CartItems.js
// The following style puts <CartItems> element on top of the page.
export const overlayStyle = {
  position: "absolute",
  backgroundColor: '#efefef',
  color: 'black',
  width: '300px',
  height: 'auto',
  top: '80px',
  left: 'calc(100% - 310px)',
  borderRadius: '5px',
  border: '1px #dfdfdf solid',
  padding: '10px',
  zIndex: "10"
}

export const cartPopUpImage = {
  width: '50px',
  height: 'auto'
}

// TW0730 - Moved these styles here.
// buttonStyle and buttonText are used in 
// - /components/CounterButtons.js
export const buttonStyle = {
  display: "flex",
  listStyleType: "none",
  lineHeight: "2em",
  verticalAlign: "middle"
}

export const buttonText = {
  fontSize: "large",
  inneWidth: "1em",
  color: "green"
}

// TW0730
// This is used in:
// - /app/main/Menu.js
//  to position the input box which filters dishes by name
export const filterInputBox = {
  position: 'absolute',
  width: '10em',
  left: 'calc(90% - 11em)'
}

export const ReviewCartImg = cartPopUpImage