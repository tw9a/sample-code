const IsPhoneValid = (phone) => {
    let newStr = ''
    for (let i = 0; i < phone.length; i++) {
        if ('0123456789'.includes(phone[i])) {
            newStr += phone[i]
        }
    }
    if (newStr.length === 9) {
        return newStr
    } else if (newStr.length === 10 && newStr[0] === '0') {
        return newStr.slice(1,)
    }
    return false
}

export default IsPhoneValid