const strings = {
  "desc": {
    "en": "This is a description of the menu",
    "sv": "Detta är en beskrivning av menyn"
  },
  "menu_eatin": {
    "en": "Eat In",
    "sv": "Äta här"
  },
  "menu_takeaway": {
    "en": "Take Away",
    "sv": "Hämta"
  },
  "checkoutcomplete_thank1": {
    "en": "Thank you, ",
    "sv": "Tack, "
  },
  "checkoutcomplete_str1": {
    "en": "The following order is received and paid for.",
    "sv": "Följande order är mottagen och betald."
  },
  "checkoutcomplete_orderlist": {
    "en": "You've ordered",
    "sv": "Du har beställt"
  },
  "checkoutcomplete_finish": {
    "en": "Finish",
    "sv": "Klart"
  },
  "aboutpage": {
    "en": "About us",
    "sv": "Om oss"
  },
  "checkout_step1": {
    "en": "Step 1: Review your order",
    "sv": "Steg 1: Granska din beställning"
  },
  "checkout_step2": {
    "en": "Step 2: Contact information",
    "sv": "Steg 2: Kontaktuppgifter"
  },
  "checkout_step3": {
    "en": "Step 3: Payment",
    "sv": "Steg 3: Betalning"
  },
  "checkout_step4": {
    "en": "Step 4: Confirmation",
    "sv": "Steg 4: Bekräftelse"
  },
  "alert_valid_phone": {
    "en": "Please enter a valid phone number",
    "sv": "Ange ett giltigt telefonnummer"
  },
  "label_contact_info": {
    "en": "Contact information",
    "sv": "Kontaktuppgifter"
  },
  "label_firstname": {
    "en": "First name",
    "sv": "Förnamn"
  },
  "label_lastname": {
    "en": "Last name",
    "sv": "Efternamn"
  },
  "label_phone": {
    "en": "Phone number",
    "sv": "Telefonnummer"
  },
  "label_email": {
    "en": "Email",
    "sv": "E-post"
  },
  "label_deliverytime": {
    "en": "Delivery time",
    "sv": "Leverans tid"
  },
  "label_please_wait": {
    "en": "Please expect to wait",
    "sv": "Vänta"
  },
  "label_minutes": {
    "en": "minutes",
    "sv": "minuter"
  },
  "button_back": {
    "en": "Back",
    "sv": "Tillbaka"
  },
  "button_place_order": {
    "en": "Place order and go to payment",
    "sv": "Gå till betalning"
  },
  "msg_payment_done": {
    "en": "Payment received. Thank you!",
    "sv": "Betalning mottagen. Tack!"
  },
  "msg_please_swish": {
    "en": "Please click “Pay with Swish” and start up Swish app.",
    "sv": "Klicka på “Betala med Swish” och starta Swish-appen."
  },
  "msg_payment_underway": {
    "en": "Payment is being processed. Please wait.",
    "sv": "Betalning pågår. Vänta."
  },
  "pay_with_swith": {
    "en": "Pay with Swish",
    "sv": "Betala med Swish"
  },
  "label_items": {
    "en": "Dishes and drinks",
    "sv": "Mat och dryck"
  },
  "label_subtotal": {
    "en": "Subtotal",
    "sv": "Totalt"
  },
  "label_remove": {
    "en": "Remove these",
    "sv": "Ta bort"
  },
  "label_eat_in_instead": {
    "en": "Eat in instead",
    "sv": "Äta här istället"
  },
  "placeholder_filter": {
    "en": "Filter dishes and drinks",
    "sv": "Filtrera mat och dryck"
  },
  "label_addmessage": {
    "en": "Add a message",
    "sv": "Lägg till ett meddelande"
  },
  "label_makechanges": {
    "en": "Make changes",
    "sv": "Ändra"
  },
  "label_finishchanging": {
    "en": "Finish changing",
    "sv": "Klart"
  },
  "button_next": {
    "en": "Next",
    "sv": "Nästa"
  },
  "button_tocheckout": {
    "en": "Take me to checkout",
    "sv": "Gå till kassan"
  },
  "button_remove": {
    "en": "Remove",
    "sv": "Ta bort"
  },
  "div_footer": {
    "en": "Copyright © 2021",
    "sv": "Copyright © 2021"
  },
  "label_not_available": {
    "en": "Not available",
    "sv": "Inte tillgänglig"
  },
  "nav_home": {
    "en": "Home",
    "sv": "Hem"
  },
  "nav_about": {
    "en": "About",
    "sv": "Om oss"
  },
  "nav_menu": {
    "en": "Menu",
    "sv": "Meny"
  },
  "nav_contact": {
    "en": "Contact us",
    "sv": "Kontakta oss"
  },
}

export default strings

// import strings from '../assets/strings'
// const lang = useSelector( state=>state.lang )