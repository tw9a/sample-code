const baseUrlOption = process.env.REACT_APP_SERVER

let baseUrl

switch(baseUrlOption) {
    case "localhost":
    baseUrl = 'http://127.0.0.1:8000/api/v2/'
    break;
    case "tunnel":
    baseUrl = process.env.REACT_APP_TUNNEL
    break;
    default:
    baseUrl = process.env.REACT_APP_HEROKU
}

export default baseUrl
