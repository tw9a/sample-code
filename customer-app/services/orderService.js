import axios from 'axios'
import baseUrl from './config'

// let token = null

// TW0814
// This can be changed of course. 
// When you make any change, also change `waittime()` in
// `backend/views.py`
export const getWaitTime = async () => {
  try {
    const response = await axios.get(`${baseUrl}orders/wait`)
    console.log('getWaitTime: response.data', response.data)
  return response.data.minutes
  } catch (e) {
    console.log(e)
  }
}

export const swishRequest = async (payload) => {
  // POST to `/payments`, and the payload is minimal.
  // payload: 
  // { orderId, payeeRef, sumAmount }
  try {
    const response = await axios.post(`${baseUrl}payments`, payload)
  console.log('response data', response.data)
  return response.data
  } catch (e) {
    console.log(e)
    return false
  }
}

export const swishPaymentStatus = async (payeeRef) => {
  // GET from `/payments/status/:payeeRef`
  const response = await axios.get(`${baseUrl}payments/status/${payeeRef}`)
  return response.data
}

// This function is invoked on "Next" being clicked at "Contact Info". Return value includes:
// - orderId: the ID of this
// - payeeRef: the reference of this order to be used in `swishRequest`
export const sendOrder = async (order) => {
  // console.log('sendOrder invoked: order looks like: ', order)
  const orderCopy = {...order}
  delete orderCopy.orderId
  delete orderCopy.payeeRef
  const response = await axios.post(`${baseUrl}orders`, orderCopy)
  return response.data
}

export default swishRequest
