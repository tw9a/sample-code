import axios from 'axios'
import baseUrl from './config'

const itemsUrl = baseUrl + 'items'
const categoriesUrl = baseUrl + 'categories'
let token = null

const getAll = async() => {
  const response = await axios.get(itemsUrl)
  return response.data
}

const getOne = async(id) => {
  const response = await axios.get(`${itemsUrl}/${id}`)
  return response.data
}

const setToken = newToken => {
  token = `bearer ${newToken}`
}

const create = async(newObject) => {
  const config = {
    headers: { Authorization: token }
  }
  const response = await axios.post(itemsUrl, newObject, config)
  return response.data
}

const update = async (id, newObject) => {
  const config = {
    headers: { Authorization: token }
  }
  const response = await axios.put(`${itemsUrl}/${id}`, newObject, config)
  return response.data
}


const remove = async (id) => {
  const config = {
    headers: { Authorization: token }
  }
  const response = await axios.delete(`${itemsUrl}/${id}/`, config)
  return response.data
}

const getAllCategories = async () => {
  const response = await axios.get(categoriesUrl)
  // response.data is an array of category objects
  return response.data
}

const getItemsByCategory = async (catName) => {
  const response = await axios.get(`${categoriesUrl}/list/${catName}`)
  return response.data
}

const itemsService = {
  getAll: getAll,
  setToken: setToken,
  create: create,
  update: update,
  remove: remove,
  getOne: getOne,
  getAllCategories: getAllCategories,
  getItemsByCategory: getItemsByCategory
}

export default itemsService