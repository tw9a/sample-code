
import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk' 
import { composeWithDevTools } from 'redux-devtools-extension'

import cartReducer from './reducers/cartReducer'
import langReducer from './reducers/langReducer'
import orderReducer from './reducers/orderReducer'
import refundReducer from './reducers/refundReducer'
import ordertypeReducer from './reducers/ordertypeReducer'
import settingReducer from './reducers/settingReducer'
import itemsReducer from './reducers/itemsReducer'
import userReducer from './reducers/userReducer'
import categoriesReducer from './reducers/categoriesReducer'

const reducer = combineReducers({
  user: userReducer,
  items: itemsReducer,
  cart: cartReducer,
  lang: langReducer,
  order: orderReducer,
  ordertype: ordertypeReducer,
  refund: refundReducer,
  setting: settingReducer,
  categories: categoriesReducer
})


const store = createStore(
  reducer,
  composeWithDevTools(
    applyMiddleware(thunk)
  )
)

export default store


