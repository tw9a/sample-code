const categoriesReducer = (state = [], action) => {
  switch(action.type) {
  case 'INIT_CATEGORIES':
    return action.categories
  default:
    return state
  }
}

export default categoriesReducer

export const initCategories = (categories) => {
  return {
    type: 'INIT_CATEGORIES',
    categories
  }
}