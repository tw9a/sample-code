const langReducer = (state='sv', action) => {
  switch (action.type) {
    case 'LANG_SHIFT':
        return state === 'sv' ? 'en' : 'sv'
    case 'SV':
        return 'sv'
    case 'EN':
        return 'en'
    default: return state
  }
}

export const langChange = () => {
  return {
    type: 'LANG_SHIFT'
  }
}

export const setSv = () => {
  return {
    type: 'SV'
  }
}

export const setEn = () => {
  return {
    type: 'EN'
  }
}

export default langReducer
