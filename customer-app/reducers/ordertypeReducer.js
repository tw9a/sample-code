const ordertypeReducer = (state='eat in', action) => {
    switch (action.type) {
        case 'ORDER_TYPE_SHIFT':
            return state === 'eat in' ? 'take away' : 'eat in'
        case 'EAT_IN':
            return 'eat in'
        case 'TAKE_AWAY':
            return 'take away'
        default:
            return state
    }
}

export const ordertypeChange = () => {
    return {
        type: 'ORDER_TYPE_SHIFT'
    }
}

export const setEatIn = () => {
    return {
        type: 'EAT_IN'
    }
}

export const setTakeAway = () => {
    return {
        type: 'TAKE_AWAY'
    }
}

export default ordertypeReducer
