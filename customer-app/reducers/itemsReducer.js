// we need to initialize the state from the beginning

const itemsReducer = (state=[], action) => {
  let itemToChange, changedItem
  switch(action.type) {
  case 'INIT_ITEMS':
    return action.items.map( (item) => { return {...item, amount:0}})
  case 'CONCAT_ITEMS':
    return [...state, ...action.items]
  case 'ADD_AN_ITEM':
    if (
      state.find(
        item => item._id === action.item._id
      )
    ) {
      return state
    } else {
      return [...state, action.item]
    }
  case 'INCREMENT':
    itemToChange = state.filter( i => i._id === action.id )[0]
    changedItem = {...itemToChange, amount:itemToChange.amount + 1 }
    return state.map( item => item._id !== changedItem._id
        ? item
        : changedItem
    )
  case 'DECREMENT':
    itemToChange = state.filter( i => i._id === action.id )[0]
    changedItem = {...itemToChange, amount:itemToChange.amount - 1 }
    return state.map( item => item._id !== changedItem._id
        ? item
        : changedItem
    )
  case 'ZERO':
    itemToChange = state.find( item => item._id === action.id )
    changedItem = {...itemToChange, amount: 0 }
    return state.map( item => item._id !== changedItem._id
    ? item
    : changedItem
    )
  default:
    return state
  }
}

// creator, to create a new element to modify the states
export const itemIncrement = id => {
  return {
  type: 'INCREMENT',
  id
  }
}

export const itemDecrement = id => {
  return {
  type: 'DECREMENT',
  id
  }
}

export const itemDelete = id => {
  return {
  type: 'ZERO',
  id
  }
}

export const itemsInitialize = (items) => {
  return {
  type: 'INIT_ITEMS',
  items
  }
}

export const itemAddOne = (singleItem) => {
  return {
    type: 'ADD_AN_ITEM',
    item: singleItem
  }
}

export default itemsReducer