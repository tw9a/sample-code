const cartReducer = (state=false, action) => {
    switch (action.type) {
        case 'CART_VIEW_CHANGE':
            return !state
        default: return state
    }
}

export const cartStateChange = () => {
    return {
        type: 'CART_VIEW_CHANGE'
    }
}

export default cartReducer
