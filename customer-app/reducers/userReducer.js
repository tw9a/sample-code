

const userReducer = (state='', action) => {
    switch(action.type) {
        case 'LOGIN':
            return action.username
        case 'LOGOUT':
            return ''
        default:
            return state
    }
}


export const userLogin = username => {
    return {
        type: 'LOGIN',
        username
    }
}

export const userLogout = () => {
    return {
        type: 'LOGOUT'
    }
}

export default userReducer
