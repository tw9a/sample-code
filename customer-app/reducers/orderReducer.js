const orderReducer = (state={}, action) => {
    switch (action.type) {
        case "SET_ORDERED_ITEMS":
          const redactedItems = action.items.map(item => {
            return {
              itemId: item._id,
              count: item.amount
            }}
            )
        return {...state, orderedItems: redactedItems}
        case "SET_ADDED_MESSAGE":
        return {...state, message: action.message}
        case "SET_CONTACT_INFO":
        return {...state, customer: action.properties}
        case "SET_DELIVERY_TIME":
          if (action.ordertype === "eat in") {
            return {...state, scheduledServingTime: action.time}
          } else {
            return {...state, scheduledPickupTime: action.time}
          }
        case "SET_PAYABLE_AMOUNT":
        return {...state, payableAmount: action.howMuch}
        case "SET_ORDER_ID":
        return {...state, orderId: action.oid}
        case "SET_ORDER_TYPE":
        return {...state, servingType: action.orderTypeString}
        case "SET_PAYEE_REF":
          return {...state, payeeRef: action.payeeRef}
        case "SET_ORDER_NULL":
          return {}
        default:
        return state
    }
}

export default orderReducer

export const orderedItems = (items) => {
    return {
        type: "SET_ORDERED_ITEMS",
        items
    }
}

export const addedMessage = (message) => {
  return {
    type: "SET_ADDED_MESSAGE",
    message
  }
}

// Internal structure of `contactInfo`
// { firstname: "firstname",
// lastname: "lastname",
// phone: "telephone",
// email: "emailyosup" }
export const contactInfo = (properties) => {
  return {
    type: "SET_CONTACT_INFO",
    properties
  }
}

export const deliveryTime = (orderType, timestring) => {
  const now = new Date()
  // Assuming Stockholm timezone.
  const dateTimeString = `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()}T${timestring}:00+01`
  return {
    type: "SET_DELIVERY_TIME",
    orderType,
    time: dateTimeString
  }
}

export const setPayableAmount = (howMuch) => {
  return {
    type: "SET_PAYABLE_AMOUNT",
    howMuch
  }
}

export const setOrderType = (orderTypeString) => {
  return { 
    type: "SET_ORDER_TYPE",
    orderTypeString
  }
}

export const setOrderId = (oid) => {
  return {
    type: "SET_ORDER_ID",
    oid
  }
}

export const setPayeeRef = (payeeRef) => {
  return {
    type: "SET_PAYEE_REF",
    payeeRef
  }
}

export const setOrderNull = () => {
  return {
    type: "SET_ORDER_NULL"
  }
}