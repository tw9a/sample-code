import IconButton from '@material-ui/core/IconButton'
import shoppingBasket from '../assets/images/shopping-basket.png'

const CartIcon = (props) => {
    const divStyles = {
        width: "36px",
        height: "36px",
        color: "white",
        backgroundImage: `url(${shoppingBasket})`,
        backgroundSize: "36px 36px",
        backgroundRepeat: "no-repeat",
        paddingTop: "12px",
        fontSize: "75%",
    }

    const display = props.value || '+'

    return (
        <IconButton onClick={props.viewChange} aria-label="add to shopping cart">
            <div style={divStyles}>{display}</div>
        </IconButton>
    )
}

export default CartIcon
