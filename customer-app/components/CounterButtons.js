// This is a button-pair counter
import { useDispatch, useSelector } from 'react-redux'
import { Button } from '@material-ui/core'
import { itemIncrement, itemDecrement, itemDelete } from '../reducers/itemsReducer'
import { buttonStyle, buttonText } from '../helpers/styling'
import strings from '../assets/strings'

const CounterButtons = ({id, remove=false}) => {
  const items = useSelector( state => state.items ) 
  const item = items.find( item => item._id === id )
  const dispatch = useDispatch()
  const lang = useSelector( state => state.lang )

  const handleDecrement = (event) => {
    if(item.amount>0){
      dispatch(itemDecrement(id))
    }else{
      console.log('clicked button to decrease the item amount which is less than 1')
    }
  }

  const handleIncrement = (event) => {
    dispatch(itemIncrement(id))
  }

  const handleZero = (event) => {
    dispatch(itemDelete(id))
  }

  if (remove !== true) {
    return (
      <li style={buttonStyle}>
        <Button size="small" color="primary" onClick={handleDecrement}><span style={buttonText}>-</span></Button>
        <div key={item._id}>{item.amount}</div>
        <Button size="small" color="primary" onClick={handleIncrement}><span style={buttonText}>+</span></Button>
      </li>
    )
  } else {
    return <li style={buttonStyle}>
    <div key={item._id}>{item.amount}</div>
    <Button size="small" color="primary" onClick={handleZero}><span style={buttonText}>{strings.button_remove[lang]}</span></Button>
  </li>
  }
  
}

export default CounterButtons
