import { Link } from "react-router-dom"
import { IconButton, Button } from '@material-ui/core'
import { useSelector } from 'react-redux'
import strings from '../assets/strings.js'

const Navbar = () => {
    const lang = useSelector( state => state.lang )
    // const user = null

    return (
        <>
            <IconButton edge="start" color="inherit" aria-label="menu">
            </IconButton>
            <Button color="inherit">
                <Link to="/">{strings.nav_home[lang]}</Link>
            </Button>
            <Button color="inherit">
                <Link to="/about">{strings.nav_about[lang]}</Link>
            </Button>
            <Button color="inherit">
                <Link to="/menu">{strings.nav_menu[lang]}</Link>
            </Button>
            <Button color="inherit">
                <Link to="/contact">{strings.nav_contact[lang]}</Link>
            </Button>
        </>
    )
}

export default Navbar
