// export default Cart
import CartPopUp from './CartPopUp'
import CartIcon from './CartIcon'
import { useSelector, useDispatch} from 'react-redux'
import { cartStateChange } from '../reducers/cartReducer'

// TW0731
// Moved the cartStateChange() from child components -- CartPopUp (formerly CartItems) and CartIcon -- to here.
// Thus allowing both the CartIcon and the `Take Me to Checkout` button to toggle visibility 
const Cart = () => {
    const dispatch = useDispatch()
    const show = useSelector( state=>state.cart )
    const ordertype = useSelector( state=>state.ordertype )
    const items = useSelector( state=>state.items.filter( item=>item.amount>0 ))
    const lang = useSelector( state=>state.lang )

    let amounts // How many dishes/items the customer has ordered
    let prices // How much the customer needs to pay

    if (ordertype === "eat in") {
      amounts = items.map( item=>item.amount).reduce( (a,b)=>a+b, 0)
      prices = items.map( item=> item.amount * parseInt(item.priceEatIn)).reduce( (a,b)=>a+b, 0)
    } else {
      amounts = items.filter( item=>item.canTakeAway).map(item=>item.amount).reduce( (a,b)=>a+b, 0)
      prices = items.filter( item=>item.canTakeAway).map(item=>item.amount * parseInt(item.priceTakeAway) ).reduce( (a,b) => a+b, 0)
    }
    
    const handleViewChange = ()=> {dispatch(cartStateChange())}

    // const isCartEmpty = (items.length===0)?true:false
    const TextDescription = (lang==="en")?<div>{amounts} items • {prices} kr</div>:<div>{amounts} artiklar • {prices} kr</div>

    return (
        <div>
            <CartIcon viewChange={handleViewChange} value={amounts} />
            { show ? <CartPopUp viewChange={handleViewChange}>{TextDescription}</CartPopUp>
                   : null }
        </div>
    )
}

export default Cart
