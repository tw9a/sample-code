import strings from '../assets/strings'
import { useSelector } from 'react-redux'

const Footer = () => {
    const lang = useSelector( state => state.lang )
    return <div style={{textAlign: "center"}}>{strings.div_footer[lang]}</div>

}

export default Footer