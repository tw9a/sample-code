// an page with template of header and footer

import Header from './Header'
import Footer from './Footer'

const Page = (props) => {
    return <div>
        <Header />
        {props.children}
        <Footer />
    </div>
}

export default Page
