import strings from '../assets/strings'
import Navbar from './Navbar'
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import { withStyles } from '@material-ui/core/styles';
import Cart from './Cart'


import LocalDiningIcon from '@material-ui/icons/LocalDining';
import LocalMallIcon  from '@material-ui/icons/LocalMall';
import { Button } from '@material-ui/core';

import { useDispatch, useSelector } from 'react-redux'
import { setEn, setSv } from '../reducers/langReducer'
import { setEatIn, setTakeAway } from '../reducers/ordertypeReducer';
// import { set } from 'mongoose';
import logoPlaceholder from '../assets/images/logo-placeholder.png'


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 0,
      flexShrink: 0,
      width: 'fit-content',
      border: `1px solid ${theme.palette.divider}`,
      borderRadius: theme.shape.borderRadius,
      backgroundColor: theme.palette.background.paper,
      color: theme.palette.text.secondary,
      '& svg': {
        margin: theme.spacing(1.5),
      },
      '& hr': {
        margin: theme.spacing(0, 0.5),
      },
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }));

  const AntSwitch = withStyles((theme) => ({ // eslint-disable-line
    root: {
      width: 28,
      height: 16,
      padding: 0,
      display: 'flex',
    },
    switchBase: {
      padding: 2,
      color: theme.palette.grey[500],
      '&$checked': {
        transform: 'translateX(12px)',
        color: theme.palette.common.white,
        '& + $track': {
          opacity: 1,
          backgroundColor: theme.palette.primary.main,
          borderColor: theme.palette.primary.main,
        },
      },
    },
    thumb: {
      width: 12,
      height: 12,
      boxShadow: 'none',
    },
    track: {
      border: `1px solid ${theme.palette.grey[500]}`,
      borderRadius: 16 / 2,
      opacity: 1,
      backgroundColor: theme.palette.common.green,
    },
    checked: {},
  }))(Switch);

const Header = () => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const langState = useSelector( state=>state.lang )
    const lang = () => langState ==='sv' ? true : false // eslint-disable-line
    const ordertypeState = useSelector( state=>state.ordertype)
    const ordertype = () => ordertypeState === 'eat in' ? false : true // eslint-disable-line

    return <AppBar position="sticky">

        <Toolbar gutters="10">
          <Grid container spacing={3}>
            <Grid item xs>Tel: 018604010</Grid>
            <Grid component="label" container alignItems="center" spacing={0}>
              <Grid item><Button onClick={() => dispatch(setSv())}>SV</Button></Grid>
              <Grid item><Button onClick={() => dispatch(setEn())}>EN</Button></Grid>
            </Grid>
          </Grid>
        </Toolbar>
        <Toolbar>
            <div><img src={logoPlaceholder} alt="logo" style={ {height: "60px", width: "auto"} } /></div>
            <Typography variant="h6" className={classes.title}>
            Aikins Sushi
            </Typography>

            <Divider orientation="vertical" flexItem />
            <Navbar />
            <Divider orientation="vertical" flexItem />
            <Cart />
        </Toolbar>
        <Toolbar>
            <Typography component="div">
                <Grid component="label" container alignItems="center" spacing={1}>
                  <Grid item>
                    <Button variant="contained" color="primary" onClick={() => dispatch(setEatIn())} startIcon={<LocalDiningIcon />}>
                      {strings.menu_eatin[langState]}
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button variant="contained" color="secondary" onClick={() => dispatch(setTakeAway())} endIcon={<LocalMallIcon />}>
                    {strings.menu_takeaway[langState]}
                    </Button>
                  </Grid>
                </Grid>
            </Typography>
            
        </Toolbar>
    </AppBar>
}

export default Header
