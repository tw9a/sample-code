import { useSelector } from 'react-redux'
import CounterButtons from './CounterButtons'
import { Button, Container } from '@material-ui/core'
import { overlayStyle, cartPopUpImage } from '../helpers/styling'
import { useHistory } from "react-router-dom"
import placeholder from '../assets/images/placeholder-square.png'
import strings from '../assets/strings'

const CartPopUp = (props) => {
  
  const cartItems = useSelector( state => state.items.filter( item => 
    item.amount > 0
  ))
  // console.log('cart items', cartItems)
  
  // TW0811
  // Added variables to handle 1) info and name display in CartPopUp and 2) non-take-away items.
  const orderType = useSelector(
    state => state.ordertype
    )
    
    const lang = useSelector(
    state => state.lang
    )

  // TW0731
  // useHistory() is added to allow sending the user to the checkout page upon clicking “take me to cashier”
  const history = useHistory()
  const handleCheckOutClick = () => {
    props.viewChange()
    history.push('/checkout/review')
  }

  // TW0730
  // 1) Fixed a bug by changing item.id to item._id
  // 2) Added overlayStyle to turn the cart into an overlay layer.
  return <Container style={overlayStyle}>{
  cartItems.map( item => {
    const itemName = lang ==='en' ? item.name.en:item.name.sv
    const itemInfo = lang ==='en' ? item.description.en:item.description.sv
    const noTakeAwayNote = lang ==='en' ? "This is not available for take away.":"Denna maträtt är inte tillgänglig för take away."
    let imgUrl
    if (item.imgUrl === '') {
      imgUrl = placeholder
    } else {
      imgUrl = item.imgUrl
    }
    
    if (item.priceTakeAway === null && orderType==='take away') {
      return (<div key={item._id}><img src={imgUrl} style={cartPopUpImage} alt='' />
      {itemName} • {itemInfo}
      <span>{noTakeAwayNote}<br />
      <CounterButtons id={item._id} remove={true} />
      </span></div>)
    } else {
      return (<div key={item._id}><img src={imgUrl} style={cartPopUpImage} alt='' />
      {itemName} • {itemInfo}
      <span><CounterButtons id={item._id} /></span></div>)
    }
    
    })}
    {props.children}
    <Button variant="contained" onClick={handleCheckOutClick} >{strings.button_tocheckout[lang]}</Button>
  </Container>

}

export default CartPopUp

