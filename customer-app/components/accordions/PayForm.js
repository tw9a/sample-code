import { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { Button } from '@material-ui/core'
// 32 digits of hex number is generated using a configured version of nanoid
import { swishRequest, swishPaymentStatus } from '../../services/orderService'
import useInterval from '../../helpers/useInterval'
import strings from '../../assets/strings'

const PayForm = (props) => {
  const lang = useSelector( state=>state.lang )
  const [paymentStatus, setPaymentStatus] = useState(null)
  const [delay, setDelay] = useState(null)
  const [payMessage, setPayMessage] = useState(strings.msg_please_swish[lang] || 'Please click “Pay on Swish” and start up Swish app.')
  const payeeRef = useSelector(state=>state.order.payeeRef)

  useInterval( ()=> {
    if (payeeRef) {
    swishPaymentStatus(payeeRef).then(
      ret => {
        setPaymentStatus(ret.status)
      }
    )}
  }, delay)

  useEffect(
    ()=> {
      if (paymentStatus==="PAID") {
        setDelay(null)
        setPayMessage(strings.msg_payment_done[lang])
        props.nextStep()
      }
    }, [paymentStatus] // eslint-disable-line
  )

  const outgoingOrder = useSelector(state=>state.order)

  const handleNextClick = () => {
    const payload = {
      orderId: outgoingOrder.orderId,
      payeeRef: outgoingOrder.payeeRef,
      sumAmount: outgoingOrder.payableAmount
    }
    setPayMessage(strings.msg_payment_underway[lang])
    swishRequest(payload)
    setDelay(1000)
  }

  return (<div>
      <div>{payMessage}</div>
      <hr />
      <Button onClick={props.prevStep}>{strings.button_back[lang]}</Button>
      <Button onClick={handleNextClick}>{strings.pay_with_swith[lang]}</Button>
  </div>)
}

export default PayForm
