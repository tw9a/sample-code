import { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Button, Container, TextField } from '@material-ui/core'
import CounterButtons from '../CounterButtons'
import { ReviewCartImg } from '../../helpers/styling'
import { itemDelete } from '../../reducers/itemsReducer'
import { ordertypeChange } from '../../reducers/ordertypeReducer'
import { orderedItems, addedMessage, setOrderType, setPayableAmount } from '../../reducers/orderReducer'
import placeholder from '../../assets/images/placeholder-square.png'
import strings from '../../assets/strings'

const ReviewCart = (props) => {
  const [message, setMessage] = useState('')
  const [change, toggleChange] = useState(false)
  const dispatch = useDispatch()

  const cartItems = useSelector(
    state => state.items.filter(
      item =>  item.amount > 0
      )
    )
  const orderType = useSelector(
    state => state.ordertype
  )
    
  const lang = useSelector(
    state => state.lang
  )
  
  const cartItemsAmount = cartItems.map( item=>item.amount ).reduce( (cur, acc)=> cur+acc, 0)
  let cartItemsSubTotal 
  if (orderType==='eat in') {
    cartItemsSubTotal = cartItems.map( item=>item.amount * parseInt(item.priceEatIn) ).reduce( (cur, acc)=> cur+acc, 0)
  } else {
    // TW0811
    // If `orderType` is `take away` and there are items not available for takeway, the line below will produce NaN
    // But we don't need to deal with that scenario here, because in the if condition below, in such case, 
    // `takeAwayItemsAmount` and `takeAwayItemsSubTotal`
    // will be displayed instead. 
    cartItemsSubTotal = cartItems.map( item=>item.amount * parseInt(item.priceTakeAway) ).reduce( (cur, acc)=> cur+acc, 0)
  }
  // console.log('cartItemsSubTotal', orderType, cartItemsSubTotal)
  
  // TW0814
  // The idea is after reviewing their order, the user shouldn't
  // be able to change their order type again. Thus the amount to
  // pay will also stay the same. Now we dispatch:
  // 1. items in the cart; 2. added message;
  // 3. amount to pay; 4. order type.
  // These won't be changed again. 
  const handleClickingNext = () => {
    dispatch(orderedItems(cartItems))
    dispatch(addedMessage(message))
    dispatch(setOrderType(orderType))
    dispatch(setPayableAmount(cartItemsSubTotal))
    props.nextStep()
  }
  
  const nonTakeAwayItems = cartItems.filter(item => item.canTakeAway === false)
  const takeAwayItems = cartItems.filter(item => item.canTakeAway && item.priceTakeAway !== null)
  const takeAwayItemsAmount = takeAwayItems.map(
    item => item.amount
  ).reduce( (a,b)=>a+b, 0)
  const takeAwayItemsSubTotal = takeAwayItems.map(
    item => item.amount * parseInt(item.priceEatIn)
  ).reduce( (a,b)=>a+b, 0)

  const clearNonTakeAwayItems = () => {
    nonTakeAwayItems.forEach(
      item => { dispatch(itemDelete(item._id)) }
    )
  }

  if (orderType === "take away" && nonTakeAwayItems.length !== 0) {
    return <Container>{takeAwayItems.map(
      item => {
        const itemName = lang ==='en' ? item.name.en:item.name.sv
        const itemInfo = lang ==='en' ? item.description.en:item.description.sv

        let imgUrl
        if (item.imgUrl === '') {
          imgUrl = placeholder
        } else {
          imgUrl = item.imgUrl
        }

        if (change) {
          return <div key={item._id}>
          <img src={imgUrl} alt='' style={ReviewCartImg} /> {itemName} • {itemInfo}
           <span><CounterButtons id={item._id} /></span>
           </div>
        } else {
          return <div key={item._id}>
          <img src={imgUrl} alt='' style={ReviewCartImg} /> {itemName} • {itemInfo}• Count {item.amount} • Total price: {item.priceTakeAway * item.amount}
          </div>
        }
      }
    )
    }
    <div>{strings.label_items[lang]} {takeAwayItemsAmount} • {strings.label_subtotal[lang]} {takeAwayItemsSubTotal}</div>
    {/* TW0811 – The styling below is only displayed when there are items  */}
    <div style={{border: "1px dashed red"}}>
    {nonTakeAwayItems.map(
      item => {
        const itemName = lang ==='en' ? item.nameEn:item.nameSv
        const itemInfo = lang ==='en' ? item.description.en:item.description.sv

        return (<div key={item._id}>
        <img src={"/images/"+item.img} alt='' style={ReviewCartImg} /> {itemName} • {itemInfo}</div>)
      }
    )
    }
    <Button onClick={clearNonTakeAwayItems}>{strings.label_remove[lang]}</Button>
    <Button onClick={()=>dispatch(ordertypeChange())}>{strings.label_eat_in_instead[lang]}</Button>
    </div>
    </Container>
  } else {
    return <Container>{
      cartItems.map(
         item => {
          const itemName = lang ==='en' ? item.name.en:item.name.sv
          const itemInfo = lang ==='en' ? item.description.en:item.description.sv
          const displayPrice = orderType === "eat in"?item.priceEatIn:item.priceTakeAway

          let imgUrl
          if (item.imgUrl === '') {
            imgUrl = placeholder
          } else {
            imgUrl = item.imgUrl
          }

           if (change) {
             return <div key={item._id}>
             <img src={imgUrl} alt='' style={ReviewCartImg} /> {itemName} • {itemInfo}
              <span><CounterButtons id={item._id} /></span>
              </div>
           } else {
             return <div key={item._id}>
             <img src={imgUrl} alt='' style={ReviewCartImg} /> {itemName} • {itemInfo}• Count {item.amount} • Total price: {displayPrice * item.amount}
             </div>
           }
         }
      )}
      <div>{strings.label_items[lang]} {cartItemsAmount} • {strings.label_subtotal[lang]} {cartItemsSubTotal}</div>
      <TextField label={strings.label_addmessage[lang]} value={message} onChange={(e)=>{setMessage(e.target.value)}} />
        <Button onClick={()=>toggleChange(!change)}>{change?strings.label_finishchanging[lang]:strings.label_makechanges[lang]}</Button>
        <Button onClick={handleClickingNext}>{strings.button_next[lang]}</Button>
      </Container>
  }
}

export default ReviewCart