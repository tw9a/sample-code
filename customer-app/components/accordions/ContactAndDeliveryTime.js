import { Button } from '@material-ui/core'
import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { contactInfo, deliveryTime, setOrderId, setPayeeRef } from '../../reducers/orderReducer'
import { getWaitTime, sendOrder } from '../../services/orderService'
import isPhoneValid from '../../helpers/phoneValid'
import { itemDelete } from '../../reducers/itemsReducer'
import strings from '../../assets/strings'

const ContactInfoDeliveryTime = (props) => {
  const [waitTime, setWaitTime] = useState(30)
  const [waitTimeDisplay, setWaitTimeDisplay] = useState('00:00')
  const ordertype = useSelector(state=>state.ordertype)
  const orderInRedux = useSelector(state=>state.order)
  const order = { ...orderInRedux }
  const lang = useSelector( state=>state.lang )

  const dispatch = useDispatch()

  const hanldClickNext = (event) => {
    event.preventDefault()
    const validPhone = isPhoneValid(event.target.phone.value)

    if (!validPhone) {
      alert(strings.alert_valid_phone[lang])
      return null
    }

    orderInRedux.orderedItems.forEach(orderedItem => {
      dispatch(itemDelete(orderedItem.itemId))
    })

    const contact = {
      firstname: event.target.firstname.value,
      lastname: event.target.lastname.value,
      phone: '+46'+validPhone,
      email: event.target.email.value
    }
    
    dispatch(contactInfo(contact))
    const timestring = event.target.deliverytime.value
    dispatch(deliveryTime(ordertype, timestring))

    const now = new Date()
    const dateTimeString = `${now.getFullYear()}-${now.getMonth()+1}-${now.getDate()}T${timestring}:00+01`
    order.customer = contact
    if (ordertype === 'eat in') {
      order.servingType='eatin'
      order.scheduledServingTime = dateTimeString
    } else {
      order.servingType='takeaway'
      order.scheduledPickupTime = dateTimeString
    }
    delete order.payableAmount

    // Before sending the order, let me change the data structure in Redux.
    sendOrder(order).then(ret => {
      dispatch(setOrderId(ret._id.$oid))
      dispatch(setPayeeRef(ret.payeeRef))
    })

    props.nextStep()
  }

  const handleDefaultDeliveryTime = async ()=>{
    const waitTime = await getWaitTime()
    setWaitTime(waitTime)
    const currentTime = new Date()
    const waitTill = new Date(currentTime.getTime() + waitTime*60000)
    // console.log(typeof waitTill.getHours())
    const hours = "0" + String(waitTill.getHours())
    const minutes = "0" + String(waitTill.getMinutes())
    setWaitTimeDisplay(`${hours.slice(-2)}:${minutes.slice(-2)}`)
  }

  useEffect(
    ()=>handleDefaultDeliveryTime(), []
  )

  return (<div>
    <form onSubmit={hanldClickNext}>
      <fieldset>
        <legend>{strings.label_contact_info[lang]}</legend>
        <label>{strings.label_firstname[lang]}: <input type="text" id="firstname" required="required" /></label>
        <label>{strings.label_lastname[lang]}: <input type="text" id="lastname" required="required" /></label><br />
        <label>{strings.label_phone[lang]}: +46 <input type="text" required="required" id="phone" /></label><br />
        <label>{strings.label_email[lang]}: <input type="text" id="email" /></label>
      </fieldset>

      <fieldset>
        <legend>{strings.label_deliverytime[lang]}</legend>
        <p>{strings.label_please_wait[lang]} {waitTime} {strings.label_minutes[lang]}.</p>
        <label>
        <input type="time" id="deliverytime" required="required" value={waitTimeDisplay} onChange={(e)=>{setWaitTimeDisplay(e.target.value)}} />
        </label>
      </fieldset>

      <Button onClick={props.prevStep}>{strings.button_back[lang]}</Button>
      <Button type="submit">{strings.button_place_order[lang]}</Button>
    </form>
    </div>
  )
}

export default ContactInfoDeliveryTime