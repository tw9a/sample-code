import { Button } from '@material-ui/core'
import { useSelector, useDispatch } from 'react-redux'
import { setOrderNull } from '../../reducers/orderReducer'
import strings from '../../assets/strings'

const CheckoutComplete = (props) => {
  const order = useSelector(state=>state.order)
  const items = useSelector(state=>state.items)
  const lang = useSelector(state=>state.lang)
  const dispatch = useDispatch()

  let displayTime = ''
  if (order.scheduledPickupTime || order.scheduledServingTime) {
    const scheduledTime = order.scheduledPickupTime || order.scheduledServingTime
    displayTime = scheduledTime.toLocaleString('defalt', { year: 'numeric', month: 'short', day: 'numeric', hour: "2-digit", minute: '2-digit', second: '2-digit'})
  }

  const handleClick = () => {
    dispatch(setOrderNull())
    props.goToHome()
  }

  const displayOrderType = (!order.servingType)? ''
    : (order.servingType === 'eat in')?
        strings.menu_eatin[lang]
        : strings.menu_takeaway[lang]

  return <div>
    <div>
      <h3>{strings.checkoutcomplete_thank1[lang]} {order.customer?order.customer.firstname:''}!</h3>
      {strings.checkoutcomplete_str1[lang]}
      <strong>{displayOrderType}</strong> <br /> {displayTime}.
      <h3>{strings.checkoutcomplete_orderlist[lang]}</h3>
      {order.orderedItems?
      <ul>
      {order.orderedItems.map(item => {
        const itemObj = items.find(i => i._id === item.itemId)
        return <li key={item.itemId}>{itemObj.name[lang]} x {item.count}</li>
      })}
    </ul>:
    ''
    }
    </div>
    <div><Button onClick={handleClick}>{strings.checkoutcomplete_finish[lang]}</Button></div>
  </div>
}

export default CheckoutComplete