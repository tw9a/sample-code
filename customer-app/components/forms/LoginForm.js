// login component is used for the admin to manage the bookings, orders and archives
import { Button } from '@material-ui/core'

const LoginForm = () => {
    const handleSubmit = () => {
        
    }

    const handleUsername = (event) => {
        event.preventDefault()
        console.log(event.target.value)
    }

    const handlePasssword = (event) => {
        event.preventDefault()
        console.log(event.target.value)
    }
    return (
        <form onSubmit={handleSubmit}>
            <div>
                Username
                <input 
                    name="username"
                    type="text"
                    placeholder="username"
                    onChange={handleUsername}/>
            </div>
            <div>Password
                <input 
                    name="password"
                    type="password"
                    onChange={handlePasssword} />
            </div>
            <Button type="submit">Login</Button>
        </form>
    )


}

export default LoginForm