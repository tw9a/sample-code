// TW0731
// Didn't touch use file, because I didn't know what's it used for.
import CartItems from '../CartPopUp'

const CartForm = () => {
    const close = {
        textAlign: "right"
    }
    
    return (<form>
        <div>Cart
            <div style={close}>X</div>
        </div>
        <div>
            <CartItems />
            <div>Total:
                <div>Price:</div>
            </div>
        </div>
        <button type="submit" to="/pay">Go to cashier</button>
        <button onClick={()=>{}} to="">Continue shopping</button>
        <button onClick={()=>{}}>Clear cart</button>
    </form>)
}

export default CartForm