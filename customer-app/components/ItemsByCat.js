import Item from 'components/Item'
import { useSelector } from 'react-redux'
import strings from '../assets/strings'

const ItemsByCat = (props) => {
  const { catObj, filterBy, orderType} = props
  const allItems = useSelector(state => state.items)
  const lang = useSelector(state => state.lang)

  const itemsThisCat = allItems.filter(item=>item.cat===catObj.cat)

  const filteredItems = itemsThisCat.filter(
    item => {
      if (item.canTakeAway === false && orderType === 'take away') {
        return false
      }
      const searchString = item.name.en + item.name.sv + item.description.en + item.description.sv
      return searchString.toLowerCase().includes(filterBy.toLowerCase())
    }
  )

  return (<div><h3>{catObj.catText[lang]}</h3>
  <div style={{display: "flex", width: '70vw', flexWrap: 'wrap'}}>
    {filteredItems?filteredItems.map(
    item => <Item item={item} key={item._id} />
  ):<div>{strings.label_not_available[lang]}</div>}</div></div>)
}

export default ItemsByCat