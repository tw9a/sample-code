import CounterButtons from './CounterButtons'
import { useSelector } from 'react-redux'
import placeholderSquare from '../assets/images/placeholder-square.png'

const Item = ({item}) => {
    const lang = useSelector( state => state.lang)
    const orderType = useSelector( state => state.ordertype )
    const id = item._id
    const price = orderType === 'eat in' ? item.priceEatIn : item.priceTakeAway
    const info = lang === 'sv' ? item.description.sv : item.description.en
    const name = lang === 'sv' ? item.name.sv : item.name.en

    const imgUrl = (item.imgUrl === '') ? placeholderSquare : item.imgUrl

    const stylePlaceHolderImages = {
      width: "150px",
      height: "100px"
    }

    return (
        <div style={{width: '250px'}}>
            <div><img src={imgUrl} alt="placeholder" style={stylePlaceHolderImages} /></div>
            <div>{name}</div>
            <span>{info}</span>
            <div>{price}</div>
            <CounterButtons id={id} />
        </div> 
    )
}

export default Item