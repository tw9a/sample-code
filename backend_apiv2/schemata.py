import certifi
from mongoengine import *
from enum import Enum
from backend.config import dbstr

connect_string=dbstr
connect(host=connect_string, tlsCAFile=certifi.where())

class ItemTypes(Enum):
    DRINK = 'drink'
    DISH = 'dish'

# There are two levels of moms...
class Moms(Enum):
    HIGH = .25
    LOW = .12

class MultilingualString(EmbeddedDocument):
    sv = StringField(max_length=256)
    en = StringField(max_length=256)

# This will only be queried when react sees 'cat' and 'subCat' fields
# - it doens't affect the grouping of items. 
# - it only affects the display of information.
class Category(Document):
    cat = StringField(max_length=64)
    catText = EmbeddedDocumentField(MultilingualString)
    type = EnumField(ItemTypes)
    moms = EnumField(Moms)
    meta = { "collection": "categories" }

class Item(Document):
    # Why do we use Embedded document for name
    # and object id for category?
    name = EmbeddedDocumentField(MultilingualString)
    imgUrl = StringField(max_length=256)
    priceEatIn = DecimalField(min_value=0, max_value=9999, precision=2)
    priceTakeAway = DecimalField(min_value=0, max_value=9999, precision=2)
    canTakeAway = BooleanField()
    description = EmbeddedDocumentField(MultilingualString)
    cat = StringField(max_length=64)
    meta = { "collection": "items" }

class ServingTypes(Enum):
    EI = 'eatin'
    TA = 'takeaway'

class OrderStatus(Enum):
    # NOTE: We deliberately didn't create a 'cancelled' status
    # because I didn't want to change the frontend too much.
    CREATED = 'created'
    PAID = 'paid'
    SERVED = 'served'
    READY = 'ready'
    PICKED = 'picked'

class ItemStatus(Enum):
    READY = 'ready'
    PREPARING = 'preparing'
    REJECTED = 'rejected'
    PENDING = 'pending'

# 1104 I don't know what will happen if I define a new document for 'order entry'
#    But it's worth trying and finding out
#    If `OrderEntry` is embedded in the Order object, do we need to add
class OrderEntry(EmbeddedDocument):
    itemId = ObjectIdField()
    count = IntField(min_value=1, max_value=999)
    # unitPrice will be the appropriate value
    unitPrice = DecimalField(min_value=0, max_value=9999, precision=2)
    # Subtotal = count * unitPrice
    subtotal = DecimalField(min_value=0, max_value=9999, precision=2)
    status = EnumField(ItemStatus)

class CustomerRecord(EmbeddedDocument):
    firstname = StringField(max_length=64)
    lastname = StringField(max_length=64)
    phone = StringField(max_length=32)
    email = StringField(max_length=64)

class Order(Document):
    orderStatus  = EnumField(OrderStatus)
    servingType = EnumField(ServingTypes, required=True)
    orderNumber = IntField(min_value=1, unique=True) # Incremental integers
    scheduledServingTime = DateTimeField()
    scheduledPickupTime = DateTimeField()
    createdAt = DateTimeField() # orders POST
    servedAt = DateTimeField() # orders/num/<num> PUT
    pickedUpAt = DateTimeField() # orders/num/<num> PUT
    paidForAt = DateTimeField() # orders/num/<num> PUT
    mealReadyAt = DateTimeField() # orders/num/<num> PUT
    payeeRef = StringField() # from client
    orderedItems = EmbeddedDocumentListField(OrderEntry) # from client
    # sumAmount is the sum of subtotal of orderedItems.
    sumAmount = DecimalField(min_value=0, max_value=9999, precision=2)
    customer = EmbeddedDocumentField(CustomerRecord)
    message = StringField(max_length=125)
    meta = { "collection": "orders" }

# Payment
#    id: MongoDB ID
#    payment record: { Swish payment }
#    associated order: ObjectID
# CREATED, PAID, DECLINED, ERROR - according to Swish docs. 
# Previous version seems to have five statuses. 
# Need find out. 

class PaymentStatuses(Enum):
    INITIATED = 'INITIATED'
    CREATED = 'CREATED'
    PAID = 'PAID'
    DECLINED = 'DECLINED'
    ERROR = 'ERROR'
    CANCELLED = 'CANCELLED'

class Payment(Document):
    status = EnumField(PaymentStatuses)
    # This ref will be used to identify if a payment has come through
    payeeRef = StringField()
    # The entire Swish payment request object
    #   will be saved in this field as a dictionary (JSON).
    paymentPayload = DictField()
    location = StringField()
    meta = { "collection": "payments" }

class UserRoles(Enum):
    MANAGER = 'manager'
    CHEF = 'chef'

class StaffMember(Document):
    username = StringField(min_length=3, max_length=16)
    passwordHash = StringField()
    role = EnumField(UserRoles)
    meta = { "collection": "staff" }

class BlockedToken(Document):
    token = StringField()
    meta = { "collection": "blockedTokens" }

class WaitTime(Document):
    '''
    There should only be one document in this collection.
    '''
    minutes = IntField(min_value=0, max_value=999)
    meta = {'collection': 'waittime'}