from functools import wraps
from django.http import JsonResponse
import jwt
from ..schemata import StaffMember, BlockedToken
from django.conf import settings

def require_authorization(roles_list):
    '''Requries authorization;
    requires a valid bearer token; and
    requires that the user role matches roles_list.
    If a valid token is found and user role is valid,
    the decoded token is appended in request.META.
    '''
    
    def decorator(func):
        @wraps(func)
        def inner(request, *args, **kwargs):
            
            if 'Authorization' not in request.headers.keys():
                return JsonResponse({"error": "invalid authorization"}, status=401)

            auth = request.headers['Authorization']
            if not auth:
                return JsonResponse({"error": "authorization missing"}, status=401)

            keyword, token = auth.split()
            if keyword.lower() != 'bearer':
                return JsonResponse({"error": "invalid token"}, status=401)
            # If token is not found in BlockedToken collections
            # return value is an empty list.
            elif BlockedToken.objects(token=token):
                return JsonResponse({"error": "token is blocked"}, status=401)
            else:
                try:
                    decoded_token = jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])
                    # print(decoded_token)
                    if decoded_token['role'] not in roles_list:
                        return JsonResponse({"error": "not authorized"}, status=401)
                    else:
                        request.META['DECODED'] = decoded_token
                except Exception as e:
                    print(e)
                    return JsonResponse({"error": "invalid token"}, status=401)
            
            return func(request, *args, **kwargs)
        return inner
    return decorator