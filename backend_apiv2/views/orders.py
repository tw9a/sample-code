from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from datetime import datetime
from nanoid import generate

from .stamps import *
import json
from ..schemata import *
from ..helpers.decorators import require_authorization

@csrf_exempt
@require_authorization(['manager', 'chef'])
def list_orders(request):
    orders = Order.objects.all().to_json()
    return JsonResponse({'orders': orders})

@csrf_exempt
@require_http_methods(["POST", 'GET'])
def orders(request):
    
    if request.method == 'POST':
        order_max = Order.objects().order_by('-orderNumber').first()
        if order_max:
            next_order_number = order_max["orderNumber"] + 1
        else:
            next_order_number = 1

        input_order = json.loads(request.body.decode('utf-8'))
        print('input order: ', input_order)
        input_order['createdAt'] = datetime.now()
        input_order['orderStatus'] = 'created'
        input_order['orderNumber'] = next_order_number
        input_order['payeeRef'] = generate("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", size=20)

        ordered_items = input_order['orderedItems'].copy()
        sumAmount = 0
        for oi in ordered_items:
            ordered_item = Item.objects(id=oi['itemId']).first()
            if input_order['servingType'] == 'eatin':
                oi['unitPrice'] = ordered_item['priceEatIn']
            else:
                oi['unitPrice'] = ordered_item['priceTakeAway']
            oi['subtotal'] = oi['unitPrice'] * oi['count']
            sumAmount += oi['subtotal']
            oi['status'] = 'pending'
        input_order['orderedItems'] = ordered_items
        input_order['sumAmount'] = sumAmount

        try:
            new_order = Order(**input_order)
            saved = new_order.save()
            update_stamps('orders')
            return HttpResponse(saved.to_json(), content_type="application/json")
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=400)
    
    if request.method == 'GET':
        response = list_orders(request)
        return response

@csrf_exempt
@require_http_methods(["POST"])
@require_authorization(['manager', 'chef'])
def set_wait_time(request):
    try:
        record = WaitTime.objects().first()
        record['minutes'] = json.loads(request.body.decode('utf-8'))['minutes']
        record.save()
    except:
        minutes = json.loads(request.body.decode('utf-8'))['minutes']
        record = WaitTime(minutes=minutes)
        record.save()
    return JsonResponse({'minutes': record['minutes']})

@csrf_exempt
@require_http_methods(["POST", 'GET'])
def wait_time(request):
    if request.method == 'GET':
        try:
            minutes = WaitTime.objects().first()['minutes']
        except:
            minutes = 30
        return JsonResponse({'minutes': minutes})
    elif request.method == 'POST':
        response = set_wait_time(request)
        return response

@csrf_exempt
@require_authorization(['manager', 'chef'])
@require_http_methods(["GET"])
def list_orders_by_status(request, status):
    if status in ['created', 'paid', 'served', 'picked', 'ready']:
        selected_orders = Order.objects(orderStatus=status).to_json()
        return HttpResponse(selected_orders, content_type="application/json")
    else:
        return JsonResponse({'error': f'{status} not recognozed'}, status=403)

@csrf_exempt
@require_authorization(['manager'])
@require_http_methods(["DELETE"])
def delete_order(request, num=None, oid=None):
    try:
        if num:
            Order.objects(orderNumber=num).first().delete()
        elif oid:
            Order.objects(id=oid).first().delete()
        update_stamps('orders')
        return JsonResponse({'success': 1})
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)

@csrf_exempt
@require_authorization(['manager', 'chef'])
@require_http_methods(["PUT"])
def change_order_status(request, num=None, oid=None):
    print('change_order_status triggered')
    '''
    input: { "orderStatus": "paid"}
    '''
    input_data = json.loads(request.body.decode('utf-8'))
    new_status = input_data['orderStatus']
    if new_status in ['created', 'paid', 'served', 'picked', 'ready']:
        try:
            if num:
                order = Order.objects(orderNumber=num).first().update(orderStatus=new_status)
            elif oid:
                order = Order.objects(id=oid).first().update(orderStatus=new_status)
            update_stamps('orders')
            return JsonResponse({'success': 1})
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=400)
    else:
        return JsonResponse({'error': f'{new_status} not recognized'}, status=403)

@csrf_exempt
@require_authorization(['manager', 'chef'])
@require_http_methods(["PUT"])
def change_order_item_status(request, num=None, oid=None, item_oid=None):
    print('change_order_item_status triggered')
    input_data = json.loads(request.body.decode('utf-8'))
    new_status = input_data['itemStatus']
    if new_status in ['pending', 'ready', 'preparing', 'rejected']:
        try:
            if num:
                Order.objects(orderNumber=num, orderedItems__itemId=item_oid).update(set__orderedItems__S__status=new_status)
                # How do I query MongoDB?
            elif oid:
                Order.objects(id=oid, orderedItems__itemId=item_oid).update(set__orderedItems__S__status=new_status)
            update_stamps('orders')
            return JsonResponse({'success': 1})
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=400)

@csrf_exempt
def show_order(request, num=None, oid=None):
    print('show_order triggered')
    if request.method == 'GET':
        if num:
            order = Order.objects(orderNumber=num).to_json()
        elif oid:
            order = Order.objects(id=oid).to_json()
        else:
            return JsonResponse({'error': 'No order number or id provided'}, status=403)
        return HttpResponse(order, content_type="application/json")
    elif request.method == 'PUT':
        response = change_order_status(request, num=num, oid=oid)
        return response
    elif request.method == 'DELETE':
        response = delete_order(request, num=num, oid=oid)
        return response