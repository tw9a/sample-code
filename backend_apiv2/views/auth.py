from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from datetime import datetime

import json
import jwt
import bcrypt
from ..schemata import *
from ..helpers.decorators import require_authorization

@csrf_exempt
@require_http_methods(["GET"])
@require_authorization(['manager', 'chef'])
def auth_who(request):
    decoded_token = request.META['DECODED']
    return JsonResponse({'username': decoded_token['username'], 'role': decoded_token['role']})

@csrf_exempt
@require_http_methods(["POST"])
def auth_logout(request):
    try:
        auth = request.headers['Authorization']
        token = auth.split()[1]
        used_token = BlockedToken(token=token)
        used_token.save()
        return JsonResponse({"success": "logged out", "message": "token blocked, please remove saved token from user device"})
    except:
        return JsonResponse({"error": "logout failed"}, status=401)

@csrf_exempt
@require_http_methods(["POST"])
def auth_login(request):
    credentials = json.loads(request.body.decode())
    username = credentials['username']
    password = credentials['password']
    user_record = StaffMember.objects(username=username)
    if not user_record:
        return JsonResponse({"error": "username or password error"}, status=401)
    else:
        user_record = user_record[0]
        # print('user_record.role.value', user_record['role'].value)
        correct_password_hash = user_record['passwordHash']
        if bcrypt.checkpw(password.encode('utf-8'), correct_password_hash.encode('utf-8')):
            token = jwt.encode({'username': user_record['username'], 'role': user_record['role'].value, 'iat': datetime.now()}, settings.SECRET_KEY, algorithm='HS256')
            return JsonResponse({"success": "logged in", 'token': token})
        else:
            return JsonResponse({"error": "username or password error"}, status=401)

@csrf_exempt
@require_http_methods(["POST"])
@require_authorization(['manager', 'chef'])
def auth_reset(request):
    decoded_token = request.META['DECODED']
    token_username = decoded_token['username']
    passwords = json.loads(request.body.decode())
    input_username = passwords['username']
    old_pass = passwords['oldPass']
    new_pass = passwords['newPass']

    if token_username != input_username:
        return JsonResponse({'error': "wrong user"}, status=401)

    # Check if username is found in DB
    user_record = StaffMember.objects(username=token_username)
    if not user_record:
        return JsonResponse({"error": "username or password error"}, status=401)

    # Check if old password matches hash
    user_record = user_record[0]
    correct_password_hash = user_record['passwordHash']
    if not bcrypt.checkpw(old_pass.encode('utf-8'), correct_password_hash.encode('utf-8')):
        return JsonResponse({"error": "username or password error"}, status=401)
    
    # Generate new hash for password
    new_hash = bcrypt.hashpw(new_pass.encode('utf-8'), bcrypt.gensalt())
    # print(type(new_hash))

    # Save new hash to DB
    ret = user_record.update(passwordHash=new_hash.decode('utf-8'))
    if ret == 1:
        return JsonResponse({"success": "password reset"})
    else:
        return JsonResponse({'error': 'error'}, status=401)

@csrf_exempt
@require_http_methods(["POST"])
@require_authorization(['manager'])
def auth_create(request):
    user_info = json.loads(request.body.decode())
    username = user_info['username']
    password = user_info['password']
    role = user_info['role']
    user_record = StaffMember.objects.filter(username=username)
    if user_record:
        return JsonResponse({"error": "username already exists"}, status=401)
    else:
        passwordHash = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
        new_user = StaffMember(username=username, passwordHash=passwordHash, role=role)
        try:
            new_user.save()
            return JsonResponse({"success": "user created"})
        except Exception as err:
            return JsonResponse({"error": err.message}, status=500)

@csrf_exempt
@require_http_methods(["GET"])
@require_authorization(['manager'])
def auth_list_users(request):
    users = StaffMember.objects.all()
    redacted_users = []
    for user in users:
        redacted_users.append({'username': user['username'], 'role': user['role'].value})
    return JsonResponse({'users': redacted_users})