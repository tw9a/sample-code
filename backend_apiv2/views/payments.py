from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
# This director is not included in this sample repository
# ... as sensitive information, such as certificates, is stored there.
from backend.config import cert, key, swish_host, host, payee_alias, signing_certificate_serial_number
# import requests
import requests
import json
from .stamps import *
from ..schemata import *
from decimal import Decimal
from datetime import datetime

def generate_payload(input):
    '''
    Take frontend input and append relevant fields to generate a payload
    - It was really dumb to leave these to the frontend:
        - signiing certificate
        - callback url.
    '''

    phone_digits = ''.join([char for char in input['customer']['phone'] if char in '0123456789'])
    return {
        'payeePaymentReference': input['payeeRef'],
        'callbackUrl': host + '/api/v2/payments/status',
        'payerAlias': phone_digits,
        'payeeAlias': payee_alias,
        'amount': float(input['sumAmount']),
        'currency': 'SEK',
        'payoutType': 'PAYOUT',
        'message': '',
        'signingCertificateSerialNumber': signing_certificate_serial_number
    }

@csrf_exempt
@require_http_methods(["POST"])
def init_payment(request):
    print('Init_payment has received this payment request: ', request.body.decode('utf-8'))
    order_info = json.loads(request.body.decode('utf-8'))
    selected_order = Order.objects(id=order_info['orderId']).first()
    
    # Check if order already paid
    print('amount in order: ', float(selected_order['sumAmount']))
    print('submitted amount: ', order_info['sumAmount'])

    if 5 == True:
        return JsonResponse({'success': '1'}, safe=False)

    if selected_order['payeeRef'] != order_info['payeeRef']:
        return JsonResponse({'error': 'Payment request and order do not match.'}, safe=False)
    elif abs(float(selected_order['sumAmount']) - order_info['sumAmount']) > 0.001:
        return JsonResponse({'error': 'Submitted amount and amount in order do not match.'}, safe=False, status=401)

    # Checks if an order has been paid for
    # The payment record has to exist and the status needs to be PAID
    payment = Payment.objects(payeeRef=order_info['payeeRef']).first()
    if payment and payment.status.value == 'PAID':
        # print('Paid for check triggered')
        return JsonResponse({'success': '1', 'message': 'This order had already been paid for.'}, safe=False)

    # Generate payload
    pay_url = swish_host+'/api/v1/paymentrequests'
    payload = generate_payload(selected_order)
    payload_json = json.dumps(payload)
    
    response1 = requests.post(
        pay_url, 
        data=payload_json,
        headers={'Content-Type': 'application/json'},
        cert=(cert, key)
    )
    # print(response1.status_code)
    # print(response1.headers)
    # print(response1.content)
    
    location = response1.headers['location']
    # print('Line 76: Do we get a location? ', headers)

    payment = Payment(**{
        "status": 'INITIATED',
        "payeeRef": order_info['payeeRef'],
        "paymentPayload": payload,
        "location": location
    })
    # print('Line 83', payment)
    payment.save()
    update_stamps('payments')
    return JsonResponse({'success': '1'}, safe=False)

@csrf_exempt
@require_http_methods(["POST"])
def payment_callback(request):
    '''
    When we receive a callback:
    - we confirm it
    - update the `status` field of `Payment`
    - update the `paidForAt` filed of `Order`;
        - we query the Order object by `payeeRef`
    '''
    # print('Line 101', request.body.decode('utf-8'))
    callback = json.loads(request.body.decode('utf-8'))
    # print('Line 103', callback)
    payeeRef = callback['payeePaymentReference']
    loc_trailing = callback['id']
    location = Payment.objects(payeeRef=payeeRef).first()['location']
    # payment_rec = Payment.objects(payeeRef=payeeRef).first()
    # print(payment_rec)
    if not location.endswith(loc_trailing):
        return JsonResponse({'error': 'Payment request and callback do not match.'}, safe=False)

    if callback['status'] == 'PAID':
        update_stamps('payments')
        update_stamps('orders')
        Order.objects(payeeRef=payeeRef).update(paidForAt=datetime.now(), orderStatus='paid')

    Payment.objects(payeeRef=payeeRef).update(status=callback['status'])
    return JsonResponse({'success': '1'}, safe=False)

@csrf_exempt
@require_http_methods(["GET"])
def check_payment_status(request, payeeref):
    '''
    Allows React to query the status of a payment
    by payeeRef.
    '''
    payment = Payment.objects(payeeRef=payeeref).first()
    return JsonResponse({'status': payment.status.value}, safe=False)

def show_test_info(request):
    print(host)
    return JsonResponse({'success': '1'}, safe=False)
