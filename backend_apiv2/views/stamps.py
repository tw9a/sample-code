from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from datetime import datetime

stamps = {
    'payments': None,
    'orders': None,
    'items': None,
    'categories': None
}

def update_stamps(keyword):
    now = datetime.now()
    stamps[keyword] = hash(now)

@csrf_exempt
def get_dbhash(request, keyword):

    if keyword == 'mock':
        for k in stamps.keys():
            update_stamps(k)
        return JsonResponse({'success': 'true'})

    if keyword in stamps.keys():
        hash_value = stamps[keyword]
        return JsonResponse({'hash': str(hash_value)})
    else:
        return JsonResponse({"error": f"'{keyword}' not found"}, status=400)
