from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from datetime import datetime
from .stamps import *

import json
from ..schemata import *
from ..helpers.decorators import require_authorization

@csrf_exempt
@require_authorization(["manager"])
def create_item(request):
    '''
    Create an item as is. MongoEngine will validate against defined schema. Will not insert reference to or from Category. Our data set is small after all.
    '''
    input_item = json.loads(request.body.decode('utf-8'))
    new_item = Item(**input_item)
    try:
        new_item.save()
        update_stamps('items')
        return JsonResponse({"success": "item created"}, safe=False)
    except Exception as e:
        return JsonResponse({"error": str(e), 'input_item': json.dumps(input_item), 'new_item': json.dumps(new_item)}, status=403)

@csrf_exempt
@require_http_methods(["GET", "POST"])
def list_items(request):
    
    if request.method == 'POST':
        response = create_item(request)
        return response
    
    if request.method == 'GET':
        items = Item.objects.all().to_json()
        return HttpResponse(items, content_type='application/json')

@csrf_exempt
@require_authorization(["manager"])
def delete_item(request, id):
    # print("id in delete_item", id)
    try:
        item = Item.objects(id=id).first()
        item.delete()
        update_stamps('items')
        return JsonResponse({"sucess": "item deleted"}, safe=False)
    except Exception as e:
        return JsonResponse({"error": str(e)}, status=404)

@csrf_exempt
@require_authorization(["manager"])
def edit_item(request, id):
    try:
        item = Item.objects(id=id).first()
        input_item = json.loads(request.body.decode('utf-8'))
        item.update(**input_item)
        update_stamps('items')
        return JsonResponse({"sucess": "item updated"}, safe=False)
    except Exception as e:
        return JsonResponse({"error": str(e)}, status=403)

@csrf_exempt
@require_http_methods(["GET", "PUT", "DELETE"])
def single_item(request, id):

    if request.method == 'GET':
        # print(id)
        item = Item.objects(id=id).to_json()
        return HttpResponse(item, content_type='application/json')
    elif request.method == 'PUT':
        response = edit_item(request, id)
        return response
    elif request.method == 'DELETE':
        response = delete_item(request, id)
        return response

@require_authorization(["manager"])
def create_category(request):
    input_category = json.loads(request.body.decode('utf-8'))
    # print("input_category", input_category)
    new_category = Category(**input_category)
    try:
        new_category.save()
        update_stamps('categories')
        return JsonResponse({"sucess": "category created"}, safe=False)
    except Exception as e:
        return JsonResponse({"error": str(e)}, status=400)

@csrf_exempt
@require_http_methods(["GET", "POST"])
def list_categories(request):
    
    if request.method == 'POST':
        response = create_category(request)
        return response
    else:
        categories = Category.objects.all().to_json()
        return HttpResponse(categories, content_type='application/json')

@csrf_exempt
@require_authorization(["manager"])
@require_http_methods(["PUT", "DELETE"])
def put_delete_category(request, id):
    if request.method == 'DELETE':
        try:
            category = Category.objects(id=id).first()
            category.delete()
            update_stamps('categories')
            return JsonResponse({"sucess": "category deleted"}, safe=False)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=404)
    elif request.method == 'PUT':
        new_info = json.loads(request.body.decode('utf-8'))
        try:
            category = Category.objects(id=id).first()
            category.update(**new_info)
            update_stamps('categories')
            return JsonResponse({"sucess": "category updated"}, safe=False)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=404)

@csrf_exempt
def list_category(request, cat):
    selected_items = Item.objects(cat=cat).to_json()
    return HttpResponse(selected_items, content_type='application/json')
