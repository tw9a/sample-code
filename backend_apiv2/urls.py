from django.urls import path
from .views.auth import *
from .views.items_cats import *
from .views.orders import *
from .views.payments import *
from .views.stamps import *

urlpatterns = [
    # ENDPOINTS: Refer to ENDPOINTS-Dec15.md
    path('auth/logout', auth_logout, name='logout'),
    path('auth/login', auth_login, name='login'),
    path('auth/reset', auth_reset, name='reset'), #
    path('auth/create', auth_create, name='create'),
    path('auth/whois', auth_who, name='who'),
    path('auth/users', auth_list_users, name='list_users'),

    # ENDPOINTS: items
    path('items', list_items, name='list_items'),

    # I'm in the middle of writing docs for these endpoints.
    # Start from this:
    path('items/<str:id>', single_item, name='show_item'),
    # PUT to update an item
    # DELETE to delete an item
    # GET to list an item (redundant)

    # ENDPOINTS: categories
    path('categories', list_categories, name='list_categories'),
    # POST to create a new category
    # GET to list all categories
    path('categories/<str:id>', put_delete_category, name='show_category'),
    # PUT to update a category
    # DELETE to delete a category

    # ENDPOINTS: categories/list (list itmes by category and subcategory)
    path('categories/list/<str:cat>', list_category, name='list_category'),
    # GET to list all items in a category

    path('orders', orders, name='list_orders'),
    # POST to create a new order
        # no token required, called from client
    # GET to list all orders
        # chefs and managers only, called from admin
    path('orders/wait', wait_time, name='wait_time'),
    # POST to set a new wait time
        # chefs and managers, called from admin
    # GET to check how much time you need to wait
        # no token required; called from client

    path('orders/status/<str:status>', list_orders_by_status, name='list_order_status'),
    # GET to list all orders with a given status
    path('orders/num/<int:num>/<str:item_oid>', change_order_item_status, name='change_order_item_status'),
    path('orders/num/<int:num>', show_order, name='show_order'),
    path('orders/id/<str:oid>/<str:item_oid>', change_order_item_status, name='change_order_item_status'),
    path('orders/id/<str:oid>', show_order, name='show_order'),
    # GET to show an order
    # PUT to update an order (manager; chef)
    # DELETE to delete an order (manager only)

    path('payments', init_payment, name='init_payment'),
    # POST to create a new payment
    path('payments/status', payment_callback, name='payment_callback'),
    # POST listen for payment callback from Swish
    # - When the callback happens, `paidForAt` will be updated
    # - and the callback object is saved to `paymentRecord`.
    path('payments/status/<str:payeeref>', check_payment_status, name='show_payment'),
    # GET to show if a payment has come through
    #   this is used for the client react app.

    path('payments/test', show_test_info, name='show_test_info'),
    path('stamps/<str:keyword>', get_dbhash, name='get_dbhash')
]