type MultilingString = {
    sv: string;
    en: string
}

type MultilingStringSet = {
    [key: string]: MultilingString
}

const strings: MultilingStringSet = {
  "app_title": {
    "en": "Welcome to Aikin Sushi",
    "sv": "Välkommen till Aikin Sushi"
  },
  "button_text": {
    "en": "Click me",
    "sv": "Klicka mig"
  },
  "itemCreation_creating": {
    "en": "Creating item",
    "sv": "Skapar objekt"
  },
  "itemCreation_name": {
    "en": "Name",
    "sv": "Namn"
  },
  "itemCreation_description": {
    "en": "Description",
    "sv": "Beskrivning"
  },
  "itemCreation_canTakeAway": {
    "en": "Takeaway",
    "sv": "Upphämtning"
  },
  "itemCreation_canTakeAwayLabel": {
    "en": "Is this available for takeaway?",
    "sv": "Kan detta objekt upphämtas?"
  },
  "yes": {
    "en": "Yes",
    "sv": "Ja"
  },
  "no": {
    "en": "No",
    "sv": "Nej"
  },
  "itemCreation_category": {
    "en": "Category",
    "sv": "Kategori"
  },
  "itemCreation_categoryLabel": {
    "en": "Category of this item: ",
    "sv": "Kategori av detta objekt: "
  },
  "itemCreation_imgUrl": {
    "en": "Image URL",
    "sv": "Bild-URL"
  },
  "itemCreation_imgUrlLabel": {
    "en": "URL to an image of this item:",
    "sv": "URL till en bild av detta objekt: "
  },
  "itemCreation_prices": {
    "en": "Prices",
    "sv": "Priser"
  },
  "take_away": {
    "en": "Take away",
    "sv": "Upphämtning"
  },
  "eat_in": {
    "en": "Eat in",
    "sv": "I  restaurangen"
  },
  "button_create": {
    "en": "Create",
    "sv": "Skapa"
  },
  "alert_error_deleting_item": {
    "en": "Error deleting item",
    "sv": "Fel vid borttagning av objekt"
  },
  "item_undefined": {
    "en": "Item is undefined",
    "sv": "Objektet är odefinierat"
  },
  "item_edit_delete": {
    "en": "Edit or delete",
    "sv": "Redigera eller ta bort"
  },
  "showing_item_id": {
    "en": "Item ID: ",
    "sv": "ID för objekt: "
  },
  "update_item": {
    "en": "Update",
    "sv": "Uppdatera"
  },
  "delete_item": {
    "en": "Delete",
    "sv": "Ta bort"
  },
  "button_edit_item": {
    "en": "Edit",
    "sv": "Redigera"
  },
  "itemsGrouping_no_items_found": {
    "en": "No items found in this category",
    "sv": "Inga objekt hittades i denna kategori"
  },
  "alert_something_wrong": {
    "en": "Something went wrong",
    "sv": "Något gick fel"
  },
  "status_pending": {
    "en": "Pending",
    "sv": "Väntar"
  },
  "status_ready": {
    "en": "Ready",
    "sv": "Klar"
  },
  "status_preparing": {
    "en": "Preparing",
    "sv": "Förbereder"
  },
  "status_rejected": {
    "en": "Rejected",
    "sv": "Avslagen"
  },
  "status_paid": {
    "en": "Paid",
    "sv": "Betald"
  },
  "status_picked_up": {
    "en": "Picked up",
    "sv": "Upphämtad"
  },
  "status_served": {
    "en": "Served",
    "sv": "serverad"
  },
  "status_created": {
    "en": "Created",
    "sv": "Beställd"
  },
  "prep_for": {
    "en": "for",
    "sv": "till"
  },
  "prep_at": {
    "en": "at",
    "sv": "kl."
  },
  "order_overdue": {
    "en": "OVERDUE",
    "sv": "SENKOMMEN"
  },
  "today": {
    "en": "Today",
    "sv": "Idag"
  },
  "tomorrow": {
    "en": "Tomorrow",
    "sv": "Imorgon"
  },
  "button_change_status": {
    "en": "Change",
    "sv": "Ändra"
  },
  "alert_password_should_differ": {
    "en": "The new password needs to be different from the old.",
    "sv": "Det nya lösenordet måste vara annorlunda än det gamla."
  },
  "alert_password_changed": {
    "en": "Password changed successfully. Please login again.",
    "sv": "Lösenordet ändrades. Logga in igen."
  },
  "alert_password_not_changed": {
    "en": "Password not changed. Please try again.",
    "sv": "Lösenordet ändrades inte. Försök igen."
  },
  "placeholder_password": {
    "en": "Password",
    "sv": "Lösenord"
  },
  "placeholder_new_password": {
    "en": "New password",
    "sv": "Nytt lösenord"
  },
  "placeholder_username": {
    "en": "Username",
    "sv": "Användarnamn"
  },
  "button_change_password": {
    "en": "Change password",
    "sv": "Ändra lösenord"
  },
  "alert_user_created": {
    "en": "User created successfully.",
    "sv": "Användaren skapades."
  },
  "alert_user_not_created": {
    "en": "User not created. Please try again.",
    "sv": "Användaren skapades inte. Försök igen."
  },
  "role_manager": {
    "en": "Owner (manager)",
    "sv": "Ägare (manager)"
  },
  "role_chef": {
    "en": "Waiter (chef)",
    "sv": "Servitör (chef)"
  },
  "button_create_user": {
    "en": "Create user",
    "sv": "Skapa användare"
  },
  "list_username": {
    "en": "Username",
    "sv": "Användarnamn"
  },
  "list_role": {
    "en": "Role",
    "sv": "Roll"
  },
  "button_login": {
    "en": "Login",
    "sv": "Logga in"
  },
  "button_logout": {
    "en": "Logout",
    "sv": "Logga ut"
  },
  "alert_logout_success": {
    "en": "Logged out successfully.",
    "sv": "Utloggad."
  },
  "alert_login_failed": {
    "en": "Login failed. Please try again.",
    "sv": "Inloggningen misslyckades. Försök igen."
  },
  "logged_in_as": {
    "en": "Logged in as",
    "sv": "Inloggad som"
  },
  "not_logged_in": {
    "en": "Not logged in",
    "sv": "Inte inloggad"
  },
  "text_please_login": {
    "en": "Please login to continue.",
    "sv": "Logga in för att fortsätta."
  },
  "text_list_of_users": {
    "en": "List of users",
    "sv": "Lista över användare"
  },
  "text_create_user": {
    "en": "Create a new user",
    "sv": "Skapa en ny användare"
  },
  "text_change_password": {
    "en": "Change your own password",
    "sv": "Ändra ditt lösenord"
  },
  "nav_orders": {
    "en": "Orders",
    "sv": "Beställningar"
  },
  "nav_items": {
    "en": "Items",
    "sv": "Objekt"
  },
  "nav_users": {
    "en": "Users",
    "sv": "Användare"
  },
  "orders_pane_status_filter": {
    "en": "Filter orders by status:",
    "sv": "Filtrera beställningar efter status:"
  },
  "item_available_takeaway": {
    "en": "Available for takeaway.",
    "sv": "Tillgängligt för utlämningsbeställningar."
  },
  "item_crud_button_close": {
    "en": "Close",
    "sv": "Stäng"
  },
  "alert_bad_credentials": {
    "en": "Bad credentials.",
    "sv": "Felaktiga inloggningsuppgifter."
  }
}


export default strings