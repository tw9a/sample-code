import styled from 'styled-components'
import { createGlobalStyle } from "styled-components"

export const GlobalStyle = createGlobalStyle`
body {  
  font-family: 'Roboto', Helvetica, Arial, sans-serif;
  background-color: #fafafa;
}
`

export const LoginForm = styled.form`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 40vw;
  position: absolute;
  top: 30vh;
  left: 30vw;
  
  & input {
    padding: 3px;
    margin: 3px;
  }

  & button {
    padding: 3px;
    margin: 3px;
    width: 100px;
  }
`
export const TopRight = styled.div`
  position: absolute;
  top: 5px;
  right: 5px;
  text-align: right;
  padding: 5px;
  display: inline-block;

  & button, input {
    margin: 5px;
  }

  & span.logOutRole {
    font-weight: light;
  }

  & span.logOutUsername {
    font-weight: bold;
  }
`
export const StyledNavBar = styled.div`
  display: flex;
  justify-content: flex-start
  align-items: center;
  margin: 5px;

  & a, div {
    display: block;
    padding: 5px;
    margin: 0px;
    width: 130px;
    text-align: center;
    font-weight: bolder;
  }

  & a {
    text-decoration: none;
    border-bottom: 1px solid #ccc;
    color: #14137c;
  }

  & div {
    border-top: 1px solid #ccc;
    border-left: 1px solid #ccc;
    border-right: 1px solid #ccc;
    border-bottom: none;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
  }
`

interface PropsIsAccented {
  isAccented: boolean
}

export const StyledOrdersEntry = styled.div<PropsIsAccented>`
  background-color: ${(props) => (props.isAccented ? '#fafafa' : '#fff')};
  display: grid;
  grid-template: "left middle right" / 2.5fr 2fr 15fr;
  padding:  5px;
  flex-direction: row;
  justify-content: flex-start;
  border-radius: 5px;
  
  div {
    padding: 5px;
  }

  & .orderNumber {
    font-weight: bold;
    font-size: 200%
  }

  & .orderOverdue {
    color: red;
    font-weight: bold;
  }

  & .orderBiggerText {
    font-size: 120%;
  }

  && .orderEntryLeft {
    display: inline-block;
    grid-area: left;
  }

  && .orderEntryMiddle {
    display: inline-block;
    grid-area: middle;
  }

  && .orderEntryRight {
    display: inline-block;
    grid-area: right;
    display: flex;
    overflow: auto;
    flex: 1;
  }

  && .orderMessage {
    margin-top: 5px;
    font-size: 110%;
    border: 1px solid #c77676;
    border-radius: 5px;
    background-color: #f1d5d6;
  }
`

export const StyledOrderedItem = styled.div`
  width: 125px;
  display: flex;
  flex-direction: column;

  div {
    margin: 2px 5px 2px 5px;
  }

  && .itemCat {
    font-weight: light;
    font-size: 85%;
    font-style: italic;
  }

  && .itemName {
    font-size: 110%;
    font-weight: bold;
  }

  && .itemCount::before {
    content: "×";
  }
`

export const OrdersPaneContainer = styled.div`
  height: 92vh;
  overflow-y: auto;
`

export const StatusFilter = styled.div`
  display: flex;
  justify-content: flex-start;

  & label {
    margin-left: 5px; 
    margin-right: 5px;
  }
`

export const ItemCrudCreationRight = styled.div`
  position: absolute;
  right: 30px;
  top: 60px;
  border: 1px solid #ccc;
  border-radius: 5px;
  padding: 10px;
  background-color: #fafafa;

  & h4 {
    color: #14137c;
  }

  & form {
    display: flex;
    flex-direction: column;
  }

  & .itemCrudFormHeading {
    font-weight: bold;
    margin-top: 5px;
    margin-bottom: 5px;
  }

  & .itemCrudId {
    font-family: monospace;
  }

  & .closeButton {
    position: absolute;
    right: 15px;
    top: 15px;
  }
`

export const ItemsListingContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  height: 94vh;
  overflow-y: auto;
`

interface PropsIsAccented {
  isAccented: boolean
}

export const StyledItemEntry = styled.div<PropsIsAccented>`
  background-color: ${(props) => (props.isAccented ? '#fafafa' : '#fff')};
  margin-top: 5px;
  margin-bottom: 5px;

  & .itemEntryName {
    font-weight: bold;
  }

  & .itemEntryDescription {
    font-style: italic;
  }

  & .itemEntryLongDescription {
    font-size: 90%;
  }

  & .itemImg {
    float: right;
    height: 50px;
    width: auto;
  }
`

export const StyledItemsGrouping = styled.div`
  border-bottom: 1px solid #ccc;
  width: 65vw;
  padding-bottom: 10px;

  & .catHeading {
    font-weight: bold;
    margin-top: 5px;
    margin-bottom: 5px;
    color: #14137c;
    font-size: 110%;
  }
`

export const StyledUserPane = styled.div`
  /* display: flex; */

  & h4 {
    color: #14137c;
  }

  & table, th, td {
    border-collapse: collapse;
    border: 1px solid #ccc;
  }

  & th, td {
    padding: 5px;
  }

  & .bgWhite {
    background-color: #ffffff;
  }

  & form {
    display: flex;
    flex-direction: column;
  }

  & input, select {
    width: 150px;
  }

  & button {
    width: 120px;
    justify-self: flex-start;
  }
`
