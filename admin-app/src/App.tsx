
import { DataLoader } from './components/DataLoader'
import { useSelector } from 'react-redux'
import { RootStateType } from './store/store'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
// import strings from './assets/strings'

import LangSwitch from './components/LangSwitch'

import Page from './components/Page'
import Login from './components/users/Login'
import Logout from './components/users/Logout'

import { GlobalStyle, TopRight } from './assets/styles'

const App = () => {

  const userRole = useSelector((state: RootStateType) => state.userRole)

  return (
    <Router>
      <GlobalStyle />
      {(userRole.role)?<DataLoader />:null}
      <TopRight>
      {(userRole.role)?<><Logout /> • </>:null}<LangSwitch /></TopRight>
      {
        (!userRole.role)? <Login />
        : <>
        <Routes>
          <Route path="/" element={<Page name='orders' />} />
          <Route path="/items" element={<Page name='items' />} />
          <Route path="/users" element={<Page name='users' />} />
        </Routes>
        </>
      }
    </Router>
  )
}

export default App
