class LocalStorage {

    setToken(token: string): void {
        localStorage.setItem('BearerToken', token)
    }

    getToken(): string | null {
        const retToken = localStorage.getItem('BearerToken')
        return retToken
    }

    removeToken(): void {
        localStorage.removeItem('BearerToken')
    }
}

const BearerToken = new LocalStorage()

export default BearerToken