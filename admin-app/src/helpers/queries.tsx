import axios from 'axios'
import { credentialsType } from '../components/users/Login'
import BearerToken from './localstorage'
import { userRoleType } from '../store/userRoleReducer'


let BASEURL: string
if (process.env.REACT_APP_SERVER === 'tunnel') {
  BASEURL = 'https://dummyurl1' // This was a private URL to a tunnel.
} else {
  BASEURL = 'https://dummyurl2' // This is the URL to our backend server
}

console.log('BASEURL is: ', BASEURL)

export const dataFetcher = async (urlPartial: string) => {
  const token = BearerToken.getToken()
  const response = await axios.get(BASEURL+urlPartial, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
  if (response.data.hash) {
    return response.data.hash
  } else {
    return response.data
  }
}

export const login = async (credentials: credentialsType): Promise<boolean> => {
  console.log('login triggered', credentials)
  const loginUrl = BASEURL + 'auth/login'
  try {
    const res = await axios.post(loginUrl, credentials)
    const data = res.data
    const status = res.status
      
    if (status === 200) {
      BearerToken.setToken(data?.token)
      return true
    } else {
      return false
    }
  } catch {
    return false
  }
}

export const logout = async (): Promise<boolean> => {
  const logoutUrl = BASEURL + 'auth/logout'
  const token = BearerToken.getToken()
  // Do not left out that empty object.
  // There should be a payload as a positional argument
  try {
    const res = await axios.post(logoutUrl, {}, { headers: {
      Authorization: `Bearer ${token}`,
    }})
    const status = res.status
    if (status === 200) {
      BearerToken.removeToken()
      return true
    } else {
      return false
    }
  } catch {
    return false
  }
}

export const newPassword = async (payload: any): Promise<boolean> => {
  const token = BearerToken.getToken()
  try {
    await axios.post(BASEURL + 'auth/reset', payload, {
      headers: {
        Authorization: `Bearer ${token}`,
      }
  })
  BearerToken.removeToken()
  return true
  } catch {
    return false
  }
}

export const createNewUser = async (payload: any): Promise<boolean> => {
  const token = BearerToken.getToken()
  try {
    await axios.post(BASEURL + 'auth/create', payload, {
      headers: {
        Authorization: `Bearer ${token}`,
      }
    })
    return true
  } catch {
    return false
  }
}

export const whois = async (): Promise<userRoleType> | never => {
  const token = BearerToken.getToken()
  try {
    const res = await axios.get(BASEURL+'auth/whois', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    // console.log(res)
    return res.data
  } catch (err: any) {
    BearerToken.removeToken()
    return {
      username: '',
      role: '',
    }
  }
}

export const putItem = async (id: string, payload: any): Promise<any> => {
  // put, delete methods works with id
  const token = BearerToken.getToken()
  if (Object.keys(payload).includes('_id')) {
    delete payload._id
  }
  try {
    const res = await axios.put(BASEURL+'items/'+id, payload, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return res.data
  } catch (err: any) {
    console.log(JSON.stringify(err)) 
  }
}

export const deleteItem = async (id: string): Promise<any> => {
  const token = BearerToken.getToken()
  try {
    const res = await axios.delete(BASEURL+'items/'+id, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return res.data
  } catch (err: any) {
    console.log(JSON.stringify(err))
  }
}

export const postItem = async (payload: any): Promise<any> => {
  const token = BearerToken.getToken()
  if (Object.keys(payload).includes('_id')) {
    delete payload._id
  }
  try {
    const res = await axios.post(BASEURL+'items', payload, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    return res.data
  } catch (err: any) {
    console.log(JSON.stringify(err))
  }
}

export const changeOrderStatus = async (num: number, orderStatus: string): Promise<boolean> => {
  const token = BearerToken.getToken()
  const payload = { 'orderStatus': orderStatus }
  try {
    const res = await axios.put(BASEURL+`orders/num/${num}`, payload, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    if (res.status === 200) {
      return true
    } else {
      return false
    }
  } catch (err: any) {
    return false
  }
}

export const changeOrderItemStatus = async (num: number, itemIdStr: string, orderItemStatus: string): Promise<boolean> => {
  const token = BearerToken.getToken()
  const payload = { 'itemStatus': orderItemStatus }
  try {
    const res = await axios.put(BASEURL+`orders/num/${num}/${itemIdStr}`, payload, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    if (res.status === 200) {
      return true
    } else {
      return false
    }
  } catch {
    return false
  }
}
