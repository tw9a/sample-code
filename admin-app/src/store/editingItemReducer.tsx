type editingAction = {
  type: "SET_EDITING_ITEM" | "SET_CREATING_ITEM",
  payload: string
}

const editingItemReducer = (state = 'creating', action: editingAction) => {
  switch (action.type) {
    case 'SET_EDITING_ITEM':
      return action.payload;
    case 'SET_CREATING_ITEM':
      return 'creating';
    default:
      return state
  }
}

export default editingItemReducer

export const setEditingItem = (itemId: string) => {
  return {
    type: 'SET_EDITING_ITEM',
    payload: itemId
  }
}

export const setCreatingItem = () => {
  return {
    type: 'SET_CREATING_ITEM',
    payload: 'creating'
  }
}
