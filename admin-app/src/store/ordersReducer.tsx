export type orderItemStatus = 'ready' | 'preparing' | 'rejected' | 'pending'

export type orderItem = {
    itemId: {
        $oid: string
    },
    count: number,
    unitPrice: number,
    subtotal: number
    status: orderItemStatus
}

type customerInfo = {
    firstname: string,
    lastname: string,
    phone: string,
    email: string,
}

export type orderObject = {
    orderStatus: string,
    servingType: string,
    orderNumber: number,
    scheduledServingTime?: {
        $date: number
    },
    scheduledPickupTime?: {
        $date: number
    },
    createdAt: {
        $date: number
    },
    servedAt?: {
        $date: number
    },
    pickedUpAt?: {
        $date: number
    },
    paidForAt?: {
        $date: number
    },
    mealReadyAt?: {
        $date: number
    },
    payeeRef: string,
    orderedItems: orderItem[],
    sumAmount: number,
    customer: customerInfo,
    message?: string
}

type ordersState = orderObject[]

// DON'T write too many actions. Because when we update the status of an order, we do it with the server.

// Define types for actions
type ordersActionTypes = {
    type: string,
    payload: any
}

// After POSTing to the server, we fetch the entire state of orders again from the server. And update the Redux.
// Thinking: Since we'd refetch everything from the server, we only need one action: populate

const initialState: ordersState = []

const ordersReducer = (state: ordersState = initialState, action: ordersActionTypes) => {
    switch (action.type) {
        case 'POPULATE_ORDERS':
            return action.payload
        case 'UPDATE_ORDER_STATUS':
            // There must be an easier way to 
            // implement this dispatches.
            const onum = action.payload.orderNum
            const newStatus = action.payload.newStatus
            const stateTheRest = state.filter(order => order.orderNumber !== onum)
            const thisOrder = state.filter(order => order.orderNumber === onum)[0]
            thisOrder.orderStatus = newStatus
            return [...stateTheRest, thisOrder]
        case 'UPDATE_ORDER_ITEM_STATUS':
            const num = action.payload.orderNum
            const iid = action.payload.orderItemId
            const newItemStatus = action.payload.newStatus
            const stateOtherOrders = state.filter(order => order.orderNumber !== num)
            const selectedOrder = state.filter(order => order.orderNumber === num)[0]
            const newOrderedItems = selectedOrder.orderedItems
            const newOrderedItemsOthers = newOrderedItems.filter(item => item.itemId.$oid !== iid)
            // console.log('newOrderedItemsOthers', newOrderedItemsOthers)
            const newOrderedItemWorkingOn = newOrderedItems.filter(item => item.itemId.$oid === iid)[0]
            // console.log(newOrderedItemWorkingOn)
            newOrderedItemWorkingOn.status = newItemStatus
            selectedOrder.orderedItems = [...newOrderedItemsOthers, newOrderedItemWorkingOn]
            return [...stateOtherOrders, selectedOrder]
        default:
            return state
    }
}

export default ordersReducer

export const populateOrders = (orders: ordersState) => {
    return {
        type: 'POPULATE_ORDERS',
        payload: orders
    }
}

// NOTE: These are order numbers, not order IDs.
// I'm tired of my earlier laziness and gave up on using OIDs.
export const updateOrderStatus = (orderNum: number, newStatus: string) => {
    return {
        type: 'UPDATE_ORDER_STATUS',
        payload: {
            orderNum,
            newStatus
        }
    }
}

export const updateOrderItemStatus = (orderNum: number, orderItemId: string, newStatus: string) => {
    return {
        type: 'UPDATE_ORDER_ITEM_STATUS',
        payload: {
            orderNum,
            orderItemId,
            newStatus
        }
    }
}