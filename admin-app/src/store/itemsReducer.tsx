export type SingleItemPayload = {
  canTakeAway: boolean,
  cat: string,
  description: {
    sv?: string,
    en?: string
  },
  imgUrl: string,
  name: {
    sv?: string,
    en?: string,
  },
  priceEatIn: number,
  priceTakeAway: number,
  subCat: string,
  _id: {
    $oid: string
  }
}

type ItemsPayload = SingleItemPayload[]

type AddItemAction = {
  type: "ADD_ITEM",
  payload: SingleItemPayload
}

type RemoveItemAction = {
  type: "REMOVE_ITEM",
  payload: string
}

type PopulateAction = {
  type: "POPULATE_ITEMS",
  payload: ItemsPayload
}

type UpdateItemAction = {
  type: "UPDATE_ITEM",
  payload: SingleItemPayload
}


type ItemsAction = AddItemAction | RemoveItemAction | PopulateAction | UpdateItemAction

const itemsReducer = (state: ItemsPayload = [], action: ItemsAction) => {
  switch (action.type) {
  case 'ADD_ITEM':
    return [...state, action.payload];
  case 'REMOVE_ITEM':
    return state.filter(item => item['_id']['$oid'] !== action.payload);
  case 'POPULATE_ITEMS':
    return action.payload;
  case 'UPDATE_ITEM':
    return state.map(item => (item['_id']['$oid'] !== action.payload['_id']['$oid'])? item : action.payload);
  default:
    return state;
  }
}

export const addItem = (item: SingleItemPayload): AddItemAction => {
  return {
    type: "ADD_ITEM",
    payload: item
  }
}

export const removeItem = (id: string): RemoveItemAction => {
  return {
    type: "REMOVE_ITEM",
    payload: id
  }
}

export const populateItems = (items: ItemsPayload): PopulateAction => {
  return {
    type: "POPULATE_ITEMS",
    payload: items
  }
}

export const updateItem = (item: SingleItemPayload): UpdateItemAction => {
  return {
    type: "UPDATE_ITEM",
    payload: item
  }
}

export default itemsReducer