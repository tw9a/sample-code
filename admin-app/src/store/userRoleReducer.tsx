export type userRoleType = {
  username: string | null
  role: string | null
}

type userLoginType = {
  type: "LOGGEDIN"
  userinfo: userRoleType
}

type userLogoutType = {
  type: "CLEARINFO"
}

type userReducerActions = userLoginType | userLogoutType

// Reducer
const userRoleReducer = (state: userRoleType = { username: null, role: null}, action: userReducerActions) => {
    switch(action.type) {
      case "LOGGEDIN":
        return action.userinfo
      case "CLEARINFO":
        return { username: null, role: null}
      default:
        return state
    }
  }

// Actions or are they called dispatches
export const userLogin = (userRole: userRoleType): userReducerActions => {
  return {
    type: "LOGGEDIN",
    userinfo: userRole
  }
}

export const userLogout = (): userReducerActions => {
  return {
    type: "CLEARINFO"
  }
}

export default userRoleReducer