import { combineReducers, createStore } from 'redux'
import { createSelectorHook } from 'react-redux'
import { composeWithDevTools } from 'redux-devtools-extension'

// I don't think we need thunk in this app.
// When trying it out, I decided to comment the following lines
import { applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import userRoleReducer from './userRoleReducer'
import hashReducer from './hashesReducer'
import itemsReducer from './itemsReducer'
import catsReducer from './catsReducer'
import ordersReducer from './ordersReducer'
import langReducer from './langReducer'
import editingItemReducer from './editingItemReducer'

// Combine Reducers indeed work like this.
const rootReducer = combineReducers({
  userRole: userRoleReducer,
  hash: hashReducer,
  items: itemsReducer,
  cats: catsReducer,
  orders: ordersReducer,
  lang: langReducer,
  editingItem: editingItemReducer,
})

// @ts-ignore
const store = createStore(rootReducer, composeWithDevTools( applyMiddleware(thunk) ))

export default store

export type RootStateType = ReturnType<typeof rootReducer>
export const useSelectorWithType = createSelectorHook<RootStateType>()