export type stampKeys = 'payments' | 'orders' | 'items' | 'categories'

export type stampsObject = {
    [stampKey in stampKeys]: string
}

type setStampsAction = {
    type: 'ORDERS_STAMP' | 'PAYMENTS_STAMP' | 'ITEMS_STAMP' | 'CATEGORIES_STAMP',
    payload: string
}

const initialState: stampsObject = { 'payments': 'init', 'orders': 'init', 'items': 'init', 'categories': 'init' }

const stampsReducer = (state: stampsObject = initialState, action: setStampsAction) => {
    switch(action.type) {
        case "ORDERS_STAMP":
            return {
                ...state,
                orders: action.payload
            }
        case "CATEGORIES_STAMP":
            return {
                ...state,
                categories: action.payload
            }
        case "ITEMS_STAMP":
            return {
                ...state,
                items: action.payload
            }
        // Actually there's not an endpoint that lists payments, so we don't need to store the stamp
        case "PAYMENTS_STAMP":
            return {
                ...state,
                payments: action.payload
            }
        default:
            return state
    }
}

export const setOrdersStamp = (payload: string): setStampsAction => {
    return {
        type: 'ORDERS_STAMP',
        payload
    }
}

// We don't handle listing payments query for now.
// This will not be called.
export const setPaymentsStamp = (payload: string): setStampsAction => {
    return {
        type: 'PAYMENTS_STAMP',
        payload
    }
}

export const setItemsStamp = (payload: string): setStampsAction => {
    return {
        type: 'ITEMS_STAMP',
        payload
    }
}

export const setCategoriesStamp = (payload: string): setStampsAction => {
    return {
        type: 'CATEGORIES_STAMP',
        payload
    }
}

export default stampsReducer