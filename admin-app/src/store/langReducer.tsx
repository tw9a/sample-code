export type LanguageCode = 'sv' | 'en'

type setLangeAction = {
    type: 'SET_LANGUAGE',
    payload: LanguageCode
}

const langReducer = (state: LanguageCode = 'sv', action: setLangeAction) => {

  switch(action.type) {
    case 'SET_LANGUAGE':
    return action.payload
    default:
      return state
  }

}

export const setLanguage = (lang: LanguageCode) => {
  return {
    type: 'SET_LANGUAGE',
    payload: lang
  }
}

export default langReducer