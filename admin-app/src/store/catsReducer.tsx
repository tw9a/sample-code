export type CategoryObject = {
  _id: {
    $oid: string
  },
  cat: string
  type: string
  catText: {
    sv?: string
    en?: string
  },
  moms: number
}

type CategoryState = CategoryObject[]

type AddCatAction = {
  type: "ADD_CAT",
  payload: CategoryObject
}

type RemoveCatAction = {
  type: "REMOVE_CAT",
  payload: string
}

type PopulateCatAction = {
  type: "POPULATE_CAT",
  payload: CategoryState
}

type UpdateCatAction = {
  type: "UPDATE_CAT",
  payload: CategoryObject
}

type CategoryAction = AddCatAction | RemoveCatAction | PopulateCatAction | UpdateCatAction

const catsReducer = (state: CategoryState = [], action: CategoryAction) => {
  switch (action.type) {
    case "ADD_CAT":
      return [...state, action.payload]
    case "REMOVE_CAT":
      return state.filter(cat => cat._id.$oid !== action.payload)
    case "POPULATE_CAT":
      return action.payload
    case "UPDATE_CAT":
      return state.map(cat => (cat._id.$oid === action.payload._id.$oid)? action.payload : cat )
    default:
      return state
  }
}

export default catsReducer

export const addCat = (cat: CategoryObject) => {
  return {
    type: "ADD_CAT",
    payload: cat
  }
}

export const removeCat = (id: string) => {
  return {
    type: "REMOVE_CAT",
    payload: id
  }
}

export const populateCats = (cats: CategoryState) => {
  return {
    type: "POPULATE_CAT",
    payload: cats
  }
}

export const updateCat = (cat: CategoryObject) => {
  return {
    type: "UPDATE_CAT",
    payload: cat
  }
}