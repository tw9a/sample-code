import { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { CategoryObject } from '../../store/catsReducer'
import { SingleItemPayload, addItem, updateItem, removeItem } from '../../store/itemsReducer'
import { RootStateType } from '../../store/store'
import { postItem, putItem, deleteItem } from '../../helpers/queries'
import strings from '../../assets/strings'
import { setCreatingItem } from '../../store/editingItemReducer'

const initialFormState: SingleItemPayload = {
  canTakeAway: true,
  cat: '',
  description: {
    sv: '',
    en: ''
  },
  imgUrl: '',
  name: {
    sv: '',
    en: ''
  },
  priceEatIn: 0,
  priceTakeAway: 0,
  subCat: '',
  _id: {
    $oid: ''
  }
}

const ItemCrud = () => {
  const [localItem, setLocalItem] = useState<SingleItemPayload>(initialFormState)
  const dispatch = useDispatch()
  const categories = useSelector<RootStateType, CategoryObject[]>( state=>state.cats)
  const lang = useSelector<RootStateType, "sv"|"en">( state=>state.lang)
  const catNameStrings = categories.map( cat => [cat.cat, cat.catText] )
  const editingItemId = useSelector<RootStateType>( state=>state.editingItem)
  const items = useSelector<RootStateType, SingleItemPayload[]>( state=>state.items)

  // console.log('ItemCrud 39 localItem', localItem)

  useEffect(
    () => {
      if (editingItemId !== 'creating') {
      const itemObj = items.find( item => item._id.$oid === editingItemId )
      setLocalItem(itemObj as SingleItemPayload)
      }
    }, [editingItemId] // eslint-disable-line
  )

  const clearEditingItem = () => {
    setLocalItem(initialFormState)
    dispatch(setCreatingItem())
  }

  const handleValueChange = (event: React.ChangeEvent<HTMLInputElement|HTMLSelectElement>) => {
    const [name, value] = [event.target.name, event.target.value]
    // console.log('ItemCrud handleValueChange', name, value)
    if (name.startsWith('name')) {
      const eventLang = name.split('.')[1]
      // console.log(eventLang)
      const newName = {...localItem?.name}
      // @ts-ignore
      newName[eventLang] = value
      // @ts-ignore
      setLocalItem({...localItem, name: newName})
    } else if (name.startsWith('description')) {
      const eventLang = name.split('.')[1]
      const newDescription = {...localItem?.description}
      // @ts-ignore
      newDescription[eventLang] = value
      // @ts-ignore
      setLocalItem({...localItem, description: newDescription})
    } else if (name === 'canTakeAway') {
      // @ts-ignore
      setLocalItem({...localItem, canTakeAway: value === 'true'})
    } else if (name === 'priceEatIn' || name === 'priceTakeAway') {
      // @ts-ignore
      setLocalItem({...localItem, [name]: parseFloat(value)})
    } else {
      // @ts-ignore
      setLocalItem({...localItem, [name]: value})
    }
  }

  const handleDelete = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
    if (localItem !== undefined) {
      dispatch(removeItem(localItem?._id.$oid))
      deleteItem(localItem?._id.$oid)
    } else {
      alert(strings.alert_error_deleting_item[lang])
    }
    setLocalItem(initialFormState)
  }

  const handleUpdate = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
    if (localItem !== undefined) {
      dispatch(updateItem(localItem))
    }
    const payload = {...localItem}
    putItem(localItem?._id.$oid as string, payload).catch(err => console.log('handleUpdate: error: ' + JSON.stringify(err)))
  }

  const handleCreate = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
    if (localItem !== undefined) {
    const payload = {...localItem}
    postItem(payload).then( (res) => {
        dispatch(addItem(localItem))
        setLocalItem(initialFormState)
      })
    } else {
      alert(strings.item_undefined[lang])
    }
  }

    return <div>
      {(editingItemId==='creating')?<h4>{strings.itemCreation_creating[lang]}</h4>:<><h4>{strings.item_edit_delete[lang]}</h4>
      <button className="closeButton" onClick={()=>clearEditingItem()}>{strings.item_crud_button_close[lang]}</button><div>
      {strings.showing_item_id[lang]}
      <span className="itemCrudId">{localItem?._id.$oid}</span>
      </div></>}
  <form>
    <div>
    <div className="itemCrudFormHeading">{strings.itemCreation_name[lang]}</div>
    <div>
      sv: <input value={localItem?.name.sv} name='name.sv' onChange={handleValueChange} /><br />
      en: <input value={localItem?.name.en} name='name.en' onChange={handleValueChange} />
      </div>
    </div>

    <div>
    <div className="itemCrudFormHeading">{strings.itemCreation_description[lang]}</div>
    <div>
      sv: <input value={localItem?.description.sv} name='description.sv' onChange={handleValueChange} /><br />
      en: <input value={localItem?.description.en} name='description.en' onChange={handleValueChange} />
      </div>
    </div>

    <div>
    <div className="itemCrudFormHeading">
    {strings.itemCreation_canTakeAway[lang]}</div>
    <div>
      <label>{strings.itemCreation_canTakeAwayLabel[lang]}<br />
      <select value={localItem?.canTakeAway.toString()} name='canTakeAway' onChange={handleValueChange}>
        <option value="true">{strings.yes[lang]}</option>
        <option value="false">{strings.no[lang]}</option>
      </select>
      </label>
      </div>
    </div>

    <div>
    <div className="itemCrudFormHeading">
    {strings.itemCreation_category[lang]}
    </div>
    <label>{strings.itemCreation_categoryLabel[lang]}<br />
        <select value={localItem?.cat} name='cat' onChange={handleValueChange}>
          {/* @ts-ignore */}
          {catNameStrings.map( (cat, index) => <option key={index.toString()+cat[0]} value={cat[0]}>{cat[1][lang]}</option> )}
        </select>
        </label>
      </div>
    
    <div><div className="itemCrudFormHeading">
      {strings.itemCreation_imgUrl[lang]}
    </div>
    <label>{strings.itemCreation_imgUrlLabel[lang]}<br />
      <input value={localItem?.imgUrl} name='imgUrl' onChange={handleValueChange} />
      </label>
    </div>

    <div>
      <div className="itemCrudFormHeading">{strings.itemCreation_prices[lang]}</div>
    {strings.eat_in[lang]}: <input value={localItem?.priceEatIn} name='priceEatIn' onChange={handleValueChange} /><br />
    {strings.take_away[lang]}: <input value={localItem?.priceTakeAway} name='priceTakeAway' onChange={handleValueChange} />
    </div>

    {(editingItemId==='creating')?
    <button onClick={handleCreate}>{strings.button_create[lang]}</button>:<>
    <button onClick={handleUpdate}>{strings.update_item[lang]}</button>
    <button onClick={handleDelete}>{strings.delete_item[lang]}</button></>}
  </form>
  </div>
}

export default ItemCrud