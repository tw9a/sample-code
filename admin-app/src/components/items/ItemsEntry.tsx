// Issue 79 (2/2)
// This is one of two components in issue #79.
// This is child component of ItemsListing
import { LanguageCode } from '../../store/langReducer'
import { SingleItemPayload } from '../../store/itemsReducer'
import { useSelector, useDispatch } from 'react-redux'
import { RootStateType } from '../../store/store'
import { setEditingItem } from '../../store/editingItemReducer'
import strings from '../../assets/strings'
import { StyledItemEntry } from '../../assets/styles'

// TODO - Event handlers that modify or delete the item should be added. The idea is define functions in <ItemsListing> and pass in as props. (Needs more thought on this.)
type ItemsEntryProps = {
  item: SingleItemPayload
  lang: LanguageCode
  isAccented: boolean
}

const ItemsEntry = (props: ItemsEntryProps) => {
  const { item, lang, isAccented } = props;
  const localeDescription = item.description[lang]
  const localeName = item.name[lang]
  const userRole = useSelector((state: RootStateType) => state.userRole)
  const editingItem = useSelector((state: RootStateType) => state.editingItem)
  const dispatch = useDispatch()
  
  const thisItemBeingEdited = (editingItem === item._id.$oid) ? true : false

  return <StyledItemEntry isAccented={isAccented}>
    <div>
      <span className="itemEntryName">{localeName}</span>
      {localeDescription?<span className="itemEntryDescription"> • {localeDescription}</span> : null}
    </div>

    <div>{strings.itemCreation_prices[lang]}: 
      <span className="itemEntryDescription"> {strings.eat_in[lang]}</span> {item.priceEatIn}
      {(item.canTakeAway)?
        <><span className="itemEntryDescription"> • {strings.take_away[lang]}</span> {item.priceTakeAway}</>: null}
       {(item.canTakeAway)?<><div className="itemEntryLongDescription">{strings.item_available_takeaway[lang]}</div></>:null}</div>
 
    {(item.imgUrl)?<div className="itemImg"><img src={item.imgUrl} alt={localeName} /></div>:null}
  {(userRole.role==='manager' && !thisItemBeingEdited)?<button onClick={()=>dispatch(setEditingItem(item._id.$oid))}>{strings.button_edit_item[lang]}</button>:null}
  </StyledItemEntry>
}

export default ItemsEntry