import ItemsListing from './ItemsListing'
import ItemCrud from './ItemCrud'

import { useSelector } from 'react-redux'
import { RootStateType } from '../../store/store'

import { ItemCrudCreationRight } from '../../assets/styles'

const ItemsPane = () => {
  const userRole = useSelector((state: RootStateType) => state.userRole)

  return (
    <div className="items-pane">
      <ItemsListing />
      {(userRole.role==='manager')?<ItemCrudCreationRight><ItemCrud /></ItemCrudCreationRight>:null}
    </div>
  )
}

export default ItemsPane