import ItemsEntry from './ItemsEntry'
import strings from '../../assets/strings'
import { StyledItemsGrouping } from '../../assets/styles'

// Sorry future me, I used props drilling to pass down `invokeRud`
const ItemsGrouping = ({ lang, thisCat, itemsThisCat }: {lang: "sv"|'en', thisCat: any, itemsThisCat: any}) => {
  // console.log(thisCat)
  const thisCatText = thisCat.catText[lang]
  // const thisSubCatText = thisCat.subCat

  if (itemsThisCat) {
    return <StyledItemsGrouping><div className="catHeading">{thisCatText}</div>
    {
      itemsThisCat.map((item: any, idx: number) => {
        const thisKey=item._id.$oid
        const isAccented = idx%2===0
        return <ItemsEntry item={item} key={thisKey} lang={lang} isAccented={isAccented} />
      })
    }
    </StyledItemsGrouping>
  } else {
    return <StyledItemsGrouping><div className="catHeading">{thisCatText}</div>{strings.itemsGrouping_no_items_found[lang]}</StyledItemsGrouping>
  }
}

export default ItemsGrouping