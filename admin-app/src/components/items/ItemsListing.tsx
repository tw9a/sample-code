// Issue 79 (1/2)
// This is one of two components in issue #79.
// This is the parent component of ItemsEntry

import { useSelector } from 'react-redux'
import { RootStateType } from '../../store/store'
import ItemsGrouping from './ItemsGrouping'
import { ItemsListingContainer } from '../../assets/styles'

const ItemsListing = () => {
  const items = useSelector((state: RootStateType) => state.items)
  const lang = useSelector((state: RootStateType) => state.lang)
  const cats = useSelector((state: RootStateType) => state.cats)

  return <ItemsListingContainer>
    {cats.map((cat, i) => {
      const thisCatId = cat._id.$oid
      return <ItemsGrouping key={thisCatId} thisCat={cat} itemsThisCat={
        items.filter(
          item => {
            return item.cat === cat.cat
          }
          )}
          lang={lang} />}
        )
        }
  </ItemsListingContainer>;
}

export default ItemsListing