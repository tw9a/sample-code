import useSWR from 'swr'
import { useEffect } from 'react'
import { RootStateType } from '../store/store'
import { useDispatch, useSelector } from 'react-redux'
import { dataFetcher, whois } from '../helpers/queries'
import token from '../helpers/localstorage'
import { stampsObject, setOrdersStamp, setItemsStamp, setCategoriesStamp } from '../store/hashesReducer'
import { populateItems } from '../store/itemsReducer'
import { populateCats } from '../store/catsReducer'
import { populateOrders } from '../store/ordersReducer'
import { userLogin } from '../store/userRoleReducer'

export const DataLoader = () => {
  const dispatch = useDispatch()
  const stamps: stampsObject = useSelector( (state: RootStateType) => state.hash)
  const userRole = useSelector( (state: RootStateType)=>state.userRole)

  if ((!userRole.username) && token.getToken()) {
    whois().then(
      res => {
        dispatch(userLogin(res))
      }
    ).catch(
      err => {
        console.log(JSON.stringify(err))
      }
    )
  }

  // Fetch hash to check if orders or payments are updated
  const { data: ordersStamp, error: ordersStampError } = useSWR('stamps/orders', dataFetcher, {revalidateOnFocus: false, refreshInterval: 4100})
  const { data: itemsStamp, error: itemsStampError } = useSWR('stamps/items', dataFetcher, {revalidateOnFocus: false, refreshInterval: 4700})
  const { data: catsStamp, error: catsStampError } = useSWR('stamps/categories', dataFetcher, {revalidateOnFocus: false, refreshInterval: 5100})

  useEffect(
    () => {
      if (ordersStampError) {
        console.error(ordersStampError)
      }
      if (ordersStamp !== stamps['orders']) {
        dataFetcher('orders').then( (data: any) => {
          const ret = JSON.parse(data.orders)
          dispatch(populateOrders(ret))
        }).catch( (error: any) => {
          console.error(error)
        })
        dispatch(setOrdersStamp( ordersStamp ))
      }
    }, [ordersStamp, ordersStampError] //eslint-disable-line
  )

  useEffect(
    () => {
      if (itemsStampError) {
        console.error(itemsStampError)
      }
      if (itemsStamp !== stamps['items']) {
        dataFetcher('items').then( (data: any) => {
          dispatch(populateItems(data))
        }).catch(
          (error: any) => {console.error(error)}
        )
        dispatch(setItemsStamp( itemsStamp ))
      }
    }, [itemsStamp, itemsStampError] //eslint-disable-line
  )

  useEffect(
    () => {
      if (catsStampError) {
        console.error(catsStampError)
      }
      if (catsStamp !== stamps['categories']) {
        dataFetcher('categories').then( (data: any) => {
          dispatch(populateCats(data))
        })
        dispatch(setCategoriesStamp( catsStamp ))
      }
    }, [catsStamp, catsStampError] //eslint-disable-line
  )

  return <> </>
}