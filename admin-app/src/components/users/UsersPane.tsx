import { useSelector } from 'react-redux'
import { RootStateType } from '../../store/store'
import CreateNewUser from './CreateNewUser'
import ListUsers from './ListUsers'
import ChangePassword from './ChangePassword'
import strings from '../../assets/strings'
import { StyledUserPane } from '../../assets/styles'

const UsersPane = () => {
  const userRole = useSelector((state: RootStateType) => state.userRole)
  const lang = useSelector<RootStateType, "sv"|"en">(state => state.lang)

  if (userRole.role === '') {
    return <div>{strings.text_please_login[lang]}</div>
  }

  return (
    <StyledUserPane>
      {(userRole.role==='manager')?
        <><h4>{strings.text_list_of_users[lang]}</h4>
        <ListUsers />
        <h4>{strings.text_create_user[lang]}</h4>
        <CreateNewUser /></>
      :''}
      <h4>{strings.text_change_password[lang]}</h4>
      <ChangePassword />
    </StyledUserPane>
  )
}

export default UsersPane