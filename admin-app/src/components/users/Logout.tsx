import { logout } from '../../helpers/queries'
import { useDispatch, useSelector } from 'react-redux'
import { RootStateType } from '../../store/store'
import { userLogout } from '../../store/userRoleReducer'
import strings from '../../assets/strings'

const Logout = () => {
  const dispatch = useDispatch()
  const userRole = useSelector((state: RootStateType) => state.userRole)
  const lang = useSelector<RootStateType, "sv"|"en">(state => state.lang)

  const handleLogout = () => {
    logout().then(res => {
      if (res) {
        alert(strings.alert_logout_success[lang])
        dispatch(userLogout())
      } else {
        alert(strings.alert_login_failed[lang])
      }
  })}

  if (userRole.username) {
    return (
      <span><span className='logOutUsername'>{userRole.username}</span> <span className='logOutRole'>{userRole.role}</span><button onClick={handleLogout}>{strings.button_logout[lang]}</button></span>)
  } else {
    return <span>{strings.not_logged_in[lang]}</span>
  }
}

export default Logout