import { useState } from 'react'
import { login, whois } from '../../helpers/queries'
import { userLogin } from '../../store/userRoleReducer'
import { useDispatch, useSelector } from 'react-redux'
import strings from '../../assets/strings'
import { RootStateType } from '../../store/store'
import { LoginForm } from '../../assets/styles'

type credentialFields = 'username' | 'password'

export type credentialsType = {
  [key in credentialFields]: string
}

const Login = () => {
  const [credentials, setCredentials] = useState({username: '', password: ''} as credentialsType)
  const dispatch = useDispatch()
  const lang = useSelector<RootStateType, "sv"|"en">(state => state.lang)

  const handleValueChange = (event: React.ChangeEvent<HTMLInputElement>, field: credentialFields) => {
    const newCredentials: credentialsType = {...credentials}
    newCredentials[field] = event.target.value
    setCredentials(newCredentials)
  }

  const handleSubmit = (event: React.SyntheticEvent) => {
    event.preventDefault()
    // If statement there, rather than a try-catch block,
    // because queries.login() returns true or false.
    login(credentials).then(res => 
      { 
        setCredentials({username: '', password: ''})
        if (res) {
          // alert('Login Successful')
        whois().then(
          res => {
            dispatch(userLogin(res))
          }
        )} else {
          alert(strings.alert_bad_credentials[lang])
        }
      })
  }

  return <LoginForm onSubmit={handleSubmit}>
    <input type="text" id="username" value={credentials.username} onChange={(event, field="username")=>handleValueChange(event, field as credentialFields)} placeholder={strings.placeholder_username[lang]} />
    <input type="password" id="password" value={credentials.password} onChange={(event, field="password")=>handleValueChange(event, field as credentialFields)} placeholder={strings.placeholder_password[lang]} />
    <button type="submit">{strings.button_login[lang]}</button>
  </LoginForm>
}

export default Login