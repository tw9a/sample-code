import { useState, useEffect } from 'react'
import { dataFetcher } from '../../helpers/queries'
import { useSelector } from 'react-redux'
import strings from '../../assets/strings'
import { RootStateType } from '../../store/store'

type ListUserItem = {
  username: string
  role: string
}

const ListUsers = () => {
  const [users, setUsers] = useState([] as ListUserItem[])
  const lang = useSelector<RootStateType, "sv"|"en">(state => state.lang)

  useEffect(() => {
    dataFetcher('auth/users').then(data => setUsers(data.users))
  }, [])

  return (
      <table>
        <thead><tr><th>{strings.list_username[lang]}</th><th>{strings.list_role[lang]}</th></tr></thead>
        <tbody>
        {users.map( (user, idx) => {
          if (idx % 2 === 0) {
            return <tr key={user.username} className="bgWhite"><td>{user.username}</td><td>{user.role}</td></tr>
          } else {
            return <tr key={user.username}><td>{user.username}</td><td>{user.role}</td></tr>
          }
        }
      )}
        </tbody>
    </table>
  )
}

export default ListUsers