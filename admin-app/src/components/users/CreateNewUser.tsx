import { useState } from 'react';
import { createNewUser } from '../../helpers/queries';
import strings from '../../assets/strings'
import { useSelector } from 'react-redux';
import { RootStateType } from '../../store/store';

const CreateNewUser = () => {
  const lang = useSelector<RootStateType, "sv"|"en">(state => state.lang)
  const [ userinfo, setUserInfo ] = useState({
    username: '',
    password: '',
    role: 'chef',
  })

  const handleChange = (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    const { name, value } = event.target;
    setUserInfo({ ...userinfo, [name]: value })
  }

  const handleSubmit = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
    createNewUser(userinfo).then(
      (res) => {
        if (res) {
          alert(strings.alert_user_created[lang])
          setUserInfo({username: '',
          password: '',
          role: 'chef'})
        } else {
          alert(strings.alert_user_not_created[lang])
        }
      }
    )
  }

  return (<form>
    <div>
      {strings.placeholder_username[lang]}<br />
    <input type='text' name='username' onChange={handleChange} placeholder={strings.placeholder_username[lang]} value={userinfo.username} />
    </div>

    <div>
    {strings.placeholder_password[lang]}<br />
    <input type='password' name='password' onChange={handleChange} placeholder={strings.placeholder_password[lang]} value={userinfo.password} />
    </div>

    <div>
      {strings.list_role[lang]}<br />
      <select name='role' onChange={handleChange} value={userinfo.role}>
      <option value='manager'>{strings.role_manager[lang]}</option>
      <option value='chef'>{strings.role_chef[lang]}</option>
    </select>
    </div>
    

    <button onClick={handleSubmit}>{strings.button_create_user[lang]}</button>
  </form>)
}

export default CreateNewUser