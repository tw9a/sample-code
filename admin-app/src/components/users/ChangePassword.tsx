import { useState, useEffect } from 'react'
import { newPassword } from '../../helpers/queries'
import { useDispatch, useSelector } from 'react-redux'
import { userLogout } from '../../store/userRoleReducer'
import { RootStateType } from '../../store/store'
import strings from '../../assets/strings'

type resetFields = 'username' | 'oldPass' | 'newPass'

export type credentialsType = {
  [key in resetFields]: string
}

const ChangePassword = () => {
    const initialUsername = useSelector((state: RootStateType) => state.userRole.username)
    const [credentials, setCredentials] = useState({username: initialUsername || '', oldPass: '', newPass: ''} as credentialsType)
    const lang = useSelector<RootStateType, "sv"|"en">(state => state.lang)
    const dispatch = useDispatch()

    useEffect(()=>{
      if (credentials.username === '' && initialUsername) {
        setCredentials({...credentials, username: initialUsername})
    }}, [initialUsername]) //eslint-disable-line

    const handleValueChange = (event: React.ChangeEvent<HTMLInputElement>, field: resetFields) => {
    const newCredentials: credentialsType = {...credentials}
    newCredentials[field] = event.target.value
    setCredentials(newCredentials)
  }

    const handleSubmit = (event: React.SyntheticEvent) => {
    event.preventDefault()
    if (credentials.oldPass === credentials.newPass) {
        alert(strings.alert_password_should_differ[lang])
    } else {
        newPassword(credentials).then(
          res => {
            if (res) {
              alert(strings.alert_password_changed[lang])
              dispatch(userLogout())
            } else {
              alert(strings.alert_password_not_changed[lang])
            }
          }
        )
    }
  }

    return <form onSubmit={handleSubmit}>
      <div>
      {strings.placeholder_username[lang]}<br />
    <input type="text" id="username" value={credentials.username} onChange={(event, field="username")=>handleValueChange(event, field as resetFields)} placeholder={strings.placeholder_username[lang]} />
    </div>
    <div>
    {strings.placeholder_password[lang]}<br />
    <input type="password" id="password" value={credentials.oldPass} onChange={(event, field="oldPass")=>handleValueChange(event, field as resetFields)} placeholder={strings.placeholder_password[lang]} />
    </div>
    <div>
    {strings.placeholder_new_password[lang]}<br />
    <input type="password" id="newpassword" value={credentials.newPass} onChange={(event, field="newPass")=>handleValueChange(event, field as resetFields)} placeholder={strings.placeholder_new_password[lang]} />
    </div>
    
    <button type="submit">{strings.button_change_password[lang]}</button>
    
    </form>
}

export default ChangePassword