import { useDispatch, useSelector } from 'react-redux'
import { RootStateType } from '../store/store'
import { setLanguage, LanguageCode } from '../store/langReducer'

const LangSwitch = () => {
  const dispatch = useDispatch();
  const lang = useSelector((state: RootStateType) => state.lang)

  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const langCode = e.target.value as LanguageCode
    dispatch(setLanguage(langCode));
  }

  return (<span>🌍 
    <select value={lang} onChange={handleChange}>
      <option value="en">English</option>
      <option value="sv">svenska</option>
    </select>
    </span>
  );
}

export default LangSwitch