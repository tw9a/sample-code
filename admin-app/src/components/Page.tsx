import OrdersPane from './orders/OrdersPane'
import UsersPane from './users/UsersPane'
import ItemsPane from './items/ItemsPane'
import { Link } from 'react-router-dom'
import strings from '../assets/strings'
import { useSelector } from 'react-redux'

import { RootStateType } from '../store/store'
import { StyledNavBar } from '../assets/styles'

export const Page = ({ name }: {name: string}) => {
  const lang = useSelector<RootStateType, "sv"|"en">(state => state.lang)

    return (<>
      <StyledNavBar>
        {name!=='orders'?<Link to="/">{strings.nav_orders[lang]}</Link>:<div>{strings.nav_orders[lang]}</div>}
        {name!=='items'?<Link to="/items">{strings.nav_items[lang]}</Link>:<div>{strings.nav_items[lang]}</div>}
        {name!=='users'?<Link to="/users">{strings.nav_users[lang]}</Link>:<div>{strings.nav_users[lang]}</div>}
      </StyledNavBar>

      {name==='orders'?<OrdersPane />:null}
      {name==='items'?<ItemsPane />:null}
      {name==='users'?<UsersPane />:null}
    </>)
}

export default Page