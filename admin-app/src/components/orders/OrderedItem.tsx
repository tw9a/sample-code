import { useState } from 'react'
import { orderItem, updateOrderItemStatus, orderItemStatus } from '../../store/ordersReducer'
import { RootStateType } from '../../store/store'
import { useDispatch, useSelector } from 'react-redux'
import { changeOrderItemStatus } from '../../helpers/queries'
import strings from '../../assets/strings'

import { StyledOrderedItem } from '../../assets/styles'

const ChangeOrderedItemStatus = ({toggle, orderNum, itemStatus, itemIdStr}: {toggle: (val: boolean)=>void, orderNum: number, itemStatus: orderItemStatus, itemIdStr: string}) => {
  const dispatch = useDispatch()
  const [status, setStatus] = useState(itemStatus)
  const lang = useSelector<RootStateType, "sv"|"en">(state => state.lang)
  
  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setStatus(e.target.value as orderItemStatus)
  }

  const handleStatusChange = () => {
    dispatch(
      updateOrderItemStatus(
        orderNum,
        itemIdStr,
        status
      )
    )
    changeOrderItemStatus(orderNum, itemIdStr, status).then(
      res => {
        if (!res) {
          alert(strings.alert_something_wrong[lang])
        }}
    )
  }

  return <div>
    <select value={status} onChange={handleChange}>
      <option value="pending">{strings.status_pending[lang]}</option>
      <option value="ready">{strings.status_ready[lang]}</option>
      <option value="preparing">{strings.status_preparing[lang]}</option>
      <option value="rejected">{strings.status_rejected[lang]}</option>
    </select>
    <button onClick={handleStatusChange}>✅</button>
    <button onClick={()=>toggle(false)}>❌</button>
  </div>
}

const OrderedItem = ({item, orderNum}: {item: orderItem, orderNum: number} ) => {
  const [showChangeStatus, setShowChangeStatus] = useState(false)
  const { itemId, count, status } = item
  const itemInfo = useSelector( (state: RootStateType) => state.items.find(
    item => item._id.$oid === itemId.$oid
  ))
  const lang = useSelector( (state: RootStateType) => state.lang )
  const itemCat = itemInfo?.cat
  const itemName = itemInfo?.name[lang] || ''
  const itemCatDisplay = useSelector( (state: RootStateType) => state.cats.find(item=>item.cat === itemCat)?.catText[lang] ) || ''
  let itemStatusDisplay: string
  switch (status) {
    case 'pending':
      itemStatusDisplay = strings.status_pending[lang]
      break
    case 'ready':
      itemStatusDisplay = strings.status_ready[lang]
      break
    case 'preparing':
      itemStatusDisplay = strings.status_preparing[lang]
      break
    case 'rejected':
      itemStatusDisplay = strings.status_rejected[lang]
      break
    default:
      itemStatusDisplay = ''
  }

  return <StyledOrderedItem>
    <div className="itemCat">{itemCatDisplay}</div>
    <div className="itemName">{itemName}</div>
    <div className="itemCount">{count}</div>
    <div className="itemButton">{showChangeStatus?<ChangeOrderedItemStatus toggle={setShowChangeStatus} orderNum={orderNum} itemStatus={status} itemIdStr={itemId.$oid} />:<button onClick={()=>setShowChangeStatus(true)}>{itemStatusDisplay}</button>}</div>
    </StyledOrderedItem>
}

export default OrderedItem