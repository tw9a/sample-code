import { useState } from 'react'
import { useSelector } from 'react-redux'
import { RootStateType } from '../../store/store'
import OrdersEntry from './OrdersEntry'
import { nanoid } from 'nanoid'

import { orderObject } from '../../store/ordersReducer'
import { OrdersPaneContainer, StatusFilter } from '../../assets/styles'
import { OrderStatus } from './OrdersEntry'
import strings from '../../assets/strings'

type OrdersPaneState = {
  created: boolean,
  paid: boolean,
  ready: boolean,
  picked: boolean,
  served: boolean,
}

const OrdersPane = () => {
  const orders = useSelector((state: RootStateType) => state.orders)
  const lang = useSelector<RootStateType, "sv"|"en">(state => state.lang)
  const [shownStatus, setShownStatus] = useState<OrdersPaneState>({
    created: true,
    paid: true,
    ready: true,
    picked: true,
    served: true
  })

  const toggleFilterStatus = (status: OrderStatus) => {
    setShownStatus({
      ...shownStatus,
      [status]: !shownStatus[status]
    })
  }

  return <>
    <StatusFilter>
      {strings.orders_pane_status_filter[lang]}
      <label><input type="checkbox" checked={shownStatus.created} onChange={()=>toggleFilterStatus('created')} /> {strings.status_created[lang]}</label>
      <label><input type="checkbox" checked={shownStatus["paid"]} onChange={()=>toggleFilterStatus("paid")} /> {strings.status_paid[lang]}</label>
      <label><input type="checkbox" checked={shownStatus["ready"]} onChange={()=>toggleFilterStatus("ready")} /> {strings.status_ready[lang]}</label>
      <label><input type="checkbox" checked={shownStatus["picked"]} onChange={()=>toggleFilterStatus("picked")} /> {strings.status_picked_up[lang]}</label>
      <label><input type="checkbox" checked={shownStatus["served"]} onChange={()=>toggleFilterStatus("served")} /> {strings.status_served[lang]}</label>
    </StatusFilter>
  <OrdersPaneContainer>
      {orders.sort(
          (a: orderObject, b: orderObject) => b.orderNumber - a.orderNumber
      ).map((k: orderObject, idx: number) => {
        // @ts-ignore
          if (shownStatus[k.orderStatus]) {
          return <OrdersEntry key={nanoid()} order={k} accented={idx%2===0} />
          } else {
            return null
          }
      })
      }
  </OrdersPaneContainer>
  </>
}

export default OrdersPane