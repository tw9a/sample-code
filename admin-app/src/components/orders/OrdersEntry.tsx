import { useState } from 'react';
import { orderObject, updateOrderStatus } from '../../store/ordersReducer'
import { RootStateType } from '../../store/store'
import { useDispatch, useSelector } from 'react-redux'
import { isItToday, isItTomorrow, isOverdue } from '../../helpers/datedifference'
import { changeOrderStatus } from '../../helpers/queries'
import OrderedItem from './OrderedItem'
import strings from '../../assets/strings'

import { StyledOrdersEntry } from '../../assets/styles'

export type OrderStatus = 'created' | 'paid' | 'ready' | 'picked' | 'served'

const ChangeOrderStatus = ({status, toggleButton, orderNum}: {status: OrderStatus, orderNum: number, toggleButton: (val: boolean)=>void}) => {
  const [orderStatus, setOrderStatus] = useState(status)
  const lang = useSelector<RootStateType, "sv"|"en">(state => state.lang)

  const dispatch = useDispatch()
  
  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setOrderStatus(e.target.value as OrderStatus)
  }

  const handleSatatusChange = () => {
    dispatch(updateOrderStatus(orderNum, orderStatus))
    changeOrderStatus(orderNum, orderStatus).then(
      (res) => {
      if (!res) {
        alert(strings.alert_something_wrong[lang])
      }}
    )
  }
  
  return (
    <div>
    <select value={orderStatus} onChange={handleChange}>
      <option value="created">{strings.status_created[lang]}</option>
      <option value="paid">{strings.status_paid[lang]}</option>
      <option value="ready">{strings.status_ready[lang]}</option>
      <option value="picked">{strings.status_picked_up[lang]}</option>
      <option value="served">{strings.status_served[lang]}</option>
    </select>
    <br />
    <button onClick={handleSatatusChange}>✅</button>
    <button onClick={()=>toggleButton(false)}>❌</button>
    </div>
  )
}

const OrdersEntry = ({order, accented}: {order: orderObject, accented: boolean}) => {
  const lang = useSelector<RootStateType, "sv"|"en">(state => state.lang)
  const [showOrderStatus, setShowOrderStatus] = useState(false)
  const { orderNumber, servingType, orderStatus, customer, sumAmount, orderedItems } = order
  const dueTime = servingType==='eatin'? order.scheduledServingTime?.$date : order.scheduledPickupTime?.$date
  let firstname: string
  let lastname: string
  if (customer && customer.firstname) {
    firstname = customer.firstname
  } else {
    firstname = ''
  }
  
  if (customer && customer.lastname) {
    lastname = customer.lastname
  } else {
    lastname = ''
  }

  let dateRep: string // can be “tomorrow” or “today” or a date
  let timeRep: string // can be “12:00” or “12:30” or “12:00-13:00”
  let overdue: boolean = false
  if (dueTime) {
  // Note: dueTime is a Unix timestamp
  // dateTimeRep is a datetime object.
    const dueDateTime = new Date(dueTime)
    overdue = isOverdue(dueDateTime)
    if (isItToday(dueDateTime)) {
      dateRep = strings.today[lang]
    } else if (isItTomorrow(dueDateTime)) {
      dateRep = strings.tomorrow[lang]
    } else {
      const month = dueDateTime.toLocaleString('default', { month: 'short' })
      const date = dueDateTime.getDate()
      dateRep = `${month} ${date}`
    }
    const hours = '00'+dueDateTime.getHours()
    const minutes = '00'+dueDateTime.getMinutes()
    timeRep = `${hours.slice(hours.length-2, hours.length)}:${minutes.slice(minutes.length-2, minutes.length)}`
  } else {
    dateRep = ''
    timeRep = ''
  }

  let orderStatusDisplay: string
  switch (orderStatus) {
    case 'created':
      orderStatusDisplay = strings.status_created[lang]
      break
    case 'paid':
      orderStatusDisplay = strings.status_paid[lang]
      break
    case 'ready':
      orderStatusDisplay = strings.status_ready[lang]
      break
    case 'picked':
      orderStatusDisplay = strings.status_picked_up[lang]
      break
    case 'served':
      orderStatusDisplay = strings.status_served[lang]
      break
    default:
      orderStatusDisplay = ''
  }

  return <StyledOrdersEntry isAccented={accented}>
  <div className="orderEntryLeft">
    <div className="orderNumber">{orderNumber}</div>
    <div>{strings.prep_for[lang]} {firstname} {lastname}</div>
    <div>{strings.prep_at[lang]} {timeRep}<br />{dateRep}</div>
    <div>{servingType==='eatin'?strings.eat_in[lang]:strings.take_away[lang]}</div>
    {overdue?<div className="orderOverdue">{strings.order_overdue[lang]}</div>:''}
  </div>

  <div className="orderEntryMiddle">
    <div
    className="orderBiggerText">{sumAmount} kr</div>
    <div className="orderBiggerText">{orderStatusDisplay}</div>
    <div>{(!showOrderStatus)?<button onClick={()=>setShowOrderStatus(true)}>{strings.button_change_status[lang]}</button>:<ChangeOrderStatus toggleButton={setShowOrderStatus} status={orderStatus as OrderStatus} orderNum={orderNumber} />}</div>
    {(order.message)?<div className="orderMessage">{order.message}</div>:''}
  </div>

  

  <div className="orderEntryRight">{ orderedItems.sort(
    (a, b) => {
      if (a.status < b.status) {
        return -1
      } else if (a.status > b.status) {
        return 1
      } else {
        return 0
      }
    }
  ).map(
    item => <OrderedItem key={item.itemId.$oid} item={item} orderNum={orderNumber} />
  )}</div>
  </StyledOrdersEntry>
}

export default OrdersEntry